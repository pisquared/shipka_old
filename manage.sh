#!/usr/bin/env bash

set -e

function configure() {
    [ -f templates/project_config.tmp.json ] && rm templates/project_config.tmp.json
    cp templates/project_config.json.sample templates/project_config.tmp.json
    sed -i "s/EXAMPLE_APP_NAME/$PROJECT_NAME/g" templates/project_config.tmp.json
    vim templates/project_config.tmp.json
}

function bootstrap() {
    PROJECT_PATH=$1
    if [[ -z "$PROJECT_PATH" ]]; then
        echo "ERROR: Specify project path after bootstrap"
        exit 1
    else
        echo "INFO: Project path: $PROJECT_PATH"
        PROJECT_PATH=`realpath $PROJECT_PATH`
        echo "INFO: Absolute project path: $PROJECT_PATH"
        if [[ "$PROJECT_PATH" == `pwd`* ]]; then
            echo "ERROR: Project path can't be inside this directory. Exiting..."
            exit 1
        fi
        if [ -d $PROJECT_PATH ]; then
            echo "ERROR: Project path exists. Please remove or specify another. Exiting..."
            exit 1
        else
            echo "INFO: Project path doesn't exist, creating..."
            mkdir -p $PROJECT_PATH
        fi
    fi
    PROJECT_NAME=$(basename $PROJECT_PATH)
    echo "INFO: Bootstrapping project $PROJECT_NAME..."
    cp templates/.gitignore $PROJECT_PATH/.gitignore
    cp templates/hosts.ini.sample $PROJECT_PATH/hosts.ini
    cp templates/manage.sh $PROJECT_PATH/manage.sh
    cp templates/_manage.py $PROJECT_PATH/_manage.py
    cp templates/populate.py $PROJECT_PATH/populate.py
    cp templates/ansible.cfg $PROJECT_PATH/ansible.cfg

    # project config
    mkdir -p $PROJECT_PATH/etc
    if [ -f templates/project_config.tmp.json ]; then
        cp templates/project_config.tmp.json $PROJECT_PATH/etc/project_config.json
        rm templates/project_config.tmp.json
    else
        cp templates/project_config.json.sample $PROJECT_PATH/etc/project_config.json
        sed -i "s/EXAMPLE_APP_NAME/$PROJECT_NAME/g" $PROJECT_PATH/etc/project_config.json
    fi

    cp templates/etc/sensitive_config.json.sample $PROJECT_PATH/etc/sensitive_config.json
    cp templates/etc/sensitive_config.json.sample $PROJECT_PATH/etc/sensitive_config.json.dev
    cp templates/etc/sensitive_config.json.sample $PROJECT_PATH/etc/sensitive_config.json.test
    cp templates/etc/sensitive_config.json.sample $PROJECT_PATH/etc/sensitive_config.json.prod
    cp templates/etc/app_sensitive.json.sample $PROJECT_PATH/etc/app_sensitive.json
    cp templates/etc/local_config.json.sample $PROJECT_PATH/etc/local_config.json
    SHIPKA_PATH=`pwd`
    python -c "import json;json.dump({'local_shipka_path': '$SHIPKA_PATH'}, open('$PROJECT_PATH/etc/local_config.json', 'w+'))"
    cp templates/Vagrantfile $PROJECT_PATH/Vagrantfile
    cp templates/README.md $PROJECT_PATH/README.md
    sed -i "s/EXAMPLE_APP_NAME/$PROJECT_NAME/g" $PROJECT_PATH/README.md
    cp -r templates/webapp $PROJECT_PATH/webapp
    cp -r templates/features $PROJECT_PATH/features
    cp -r templates/workers $PROJECT_PATH/workers
    cp -r templates/provisioning $PROJECT_PATH/provisioning
    cp -r templates/translations $PROJECT_PATH/translations

    # FE assets config
    [ -f templates/fe_assets.tmp.json ] && rm templates/fe_assets.tmp.json
    ./scripts/bootstrap_fe_assets.py $PROJECT_PATH
    cp templates/fe_assets.tmp.json $PROJECT_PATH/webapp/fe_assets.json
    rm templates/fe_assets.tmp.json
    echo "OK: Bootstrap complete successfully!"

    # Install VirtualBox
    # Install Vagrant
    VAGRANT_VERSION_EXPECTED="Vagrant 2.0.2"
    VAGRANT_VERSION=`vagrant --version`

    if [ ! "$VAGRANT_VERSION" == "$VAGRANT_VERSION_EXPECTED" ]; then
        echo "WARN: Vagrant version different than expected. If you encounter any problems in setup, please install $VAGRANT_VERSION_EXPECTED - https://www.vagrantup.com/downloads.html"
    fi
}

command_main() {
    INITIAL_COMMAND=$1
    case "$INITIAL_COMMAND" in
        configure) configure
        ;;
        bootstrap) shift && bootstrap "$@"
        ;;
        *) >&2 echo -e "${HELP}"
        return 1
        ;;
    esac
    return $?
}

command_main "$@"