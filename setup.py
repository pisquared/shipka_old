from setuptools import setup

setup(
    name='shipka',
    version='0.3',
    packages=['shipka', 'shipka.admin', 'shipka.store', 'shipka.webapp', 'shipka.webapp.views', 'shipka.workers',],
    url='https://gitlab.com/pisquared/shipka',
    license='MIT',
    author='pisquared',
    author_email='danieltcv@gmail.com',
    description='boilerplate DevOps-oriented WebApps based on Flask'
)
