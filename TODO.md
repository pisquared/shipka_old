# TODO:
* SEC: Generate random passwords and output in gitignorable file for prod if `sensitive.json` has the default ones
* SEC: Run eventlet, celery, beat, flower, pgweb with unpriviledged, non-sudo user
* OPS: Setup Logging for Webapp / Celery / Beat
* PROD/DEVOPS: Prod_bootstrap
* DEVOPS: Don't copy `translations`, `workers`, etc. folders if not enabled in config
* DEVOPS: Simplify project config
* DOCS: Template the default passwords in the sub-project README

* DEVOPS: Monit's pid file is dependant on hostname
* DEVOPS: Enable install and save extra NPM packages
    - partly solved by project_specific, but probably also add template via manage
* DEVOPS: self.CI with GitLab
    - Test setting up blank project, vagrant up, test `manage.sh status`
* DEVOPS: project.CI with GitLab template
* OPS: Setup error tracking with sentry
    - partly setup, unstable setting up login automatically
* OPS: Setup web analytics with piwik
* OPS: Conditional setup of Monit config
* OPS: Don't copy `workers` template folder if workers are not enabled
* QA: Coverage 
* QA: Testing with lettuce
* QA: FrontEnd testing setup
* DOCS: explain `fe_assets`