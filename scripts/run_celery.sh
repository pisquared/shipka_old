#!/usr/bin/env bash
echo $$ > /var/run/celery.pid
source venv/bin/activate
celery -A shipka.workers.celery worker -l info --statedb=workers/celery_worker.state