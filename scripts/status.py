#!/usr/bin/env python
import json

import requests
import time
from colored import fg, attr
from subprocess import Popen, PIPE


def is_service_running(service):
    assert len(service.split()) == 1
    p = Popen("sudo systemctl is-active {}".format(service).split(), stdout=PIPE)
    stdout, _ = p.communicate()
    return stdout.strip() == 'active'


def service_status(is_enabled, service, desc, port=None, path='/'):
    is_enabled_color = 'green' if is_enabled else 'yellow'

    print "{service:16} {desc:25} {color} {is_enabled:8} {end_color}".format(
        service=service,
        desc=desc,
        color=fg(is_enabled_color),
        is_enabled='enabled' if is_enabled else 'not enabled',
        end_color=attr('reset')),
    if is_enabled:
        is_running = is_service_running(service)
        is_running_color = 'green' if is_running else 'red'
        print "{color} {is_running:8} {end_color}".format(
            color=fg(is_running_color),
            is_running='running' if is_running else 'not running',
            end_color=attr('reset')),
        if is_running and port:
            url = "http://localhost:{}{}".format(port, path)
            try:
                r = requests.get(url)
                is_accessible = True
                status_code = r.status_code
            except:
                status_code = 0
                is_accessible = False
            is_accessible_color = 'green' if is_accessible else 'red'
            print "{color} {is_accessible:12} {code:4} {end_color}".format(
                color=fg(is_accessible_color),
                is_accessible='accessible' if is_accessible else 'no access',
                code=status_code,
                end_color=attr('reset')),
            print url,
    print


project_config = json.load(open('etc/project_config.json'))

service_status(is_enabled=project_config.get('monitoring_mgmt_enabled'),
               port=project_config.get('monitoring_mgmt_port'),
               service="monit",
               desc="Monitoring")

service_status(is_enabled=project_config.get('webapp_proxy_enabled'),
               port=80,
               service="nginx",
               desc="Reverse proxy")

service_status(is_enabled=project_config.get('webapp_proxy_enabled'),
               port=project_config.get('app_server_local_port'),
               desc="Local server",
               service="eventlet")

service_status(is_enabled=project_config.get('database_enabled'),
               service="postgresql",
               desc="Database")

service_status(is_enabled=project_config.get('admin_webapp_enabled'),
               port=project_config.get('admin_webapp_port'),
               path='/admin',
               service="admin_webapp",
               desc="Webapp Admin")

service_status(is_enabled=project_config.get('database_mgmt_enabled'),
               port=project_config.get('database_mgmt_port'),
               service="pgweb",
               desc="Database management")

service_status(is_enabled=project_config.get('workers_enabled'),
               service="celery",
               desc="Workers")

service_status(is_enabled=project_config.get('workers_enabled'),
               service="beat",
               desc="Scheduling")

service_status(is_enabled=project_config.get('workers_mgmt_enabled'),
               port=project_config.get('workers_mgmt_port'),
               service="flower",
               desc="Workers management")

service_status(is_enabled=project_config.get('queue_mgmt_enabled'),
               port=project_config.get('queue_mgmt_port'),
               service="rabbitmq-server",
               desc="Queue management")

if project_config.get('workers_enabled'):
    resp = requests.get("http://localhost/tasks/sum/5/5")
    queue_sent = resp.status_code == 200
    is_queue_sent = 'green' if queue_sent else 'red'
    print "{padding:16} {desc:25} {color} {queue_sent:8} {end_color}".format(
        padding="",
        desc="Webapp > Queue > Wrk",
        color=fg(is_queue_sent),
        queue_sent='sent' if is_queue_sent else 'not sent',
        end_color=attr('reset')),
    result_url = "http://localhost{}".format(resp.text.split('=')[1].split("'")[1])
    time.sleep(0.2)
    resp2 = requests.get(result_url)
    task_completed = resp2.text == 'Result: 10'
    is_task_completed = 'green' if task_completed else 'red'
    print "{color} {task_completed:10} {end_color}".format(
        color=fg(is_queue_sent),
        task_completed='completed' if task_completed else 'not completed',
        end_color=attr('reset')),
