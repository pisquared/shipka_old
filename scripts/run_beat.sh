#!/usr/bin/env bash
echo $$ > /var/run/beat.pid
source venv/bin/activate
celery -A workers.schedule beat -s workers/celerybeat-schedule -l info