#!/bin/bash
sudo su
PRIMARY_USER=$1

echo "1. user create and sudo passwordless access given"
useradd $PRIMARY_USER && mkdir -p /home/$PRIMARY_USER/.ssh
echo "$PRIMARY_USER ALL=(ALL) NOPASSWD: ALL" | sudo tee --append /etc/sudoers

echo "2. install initial packages required for ansible"
apt update
apt install -y openssh-server python

echo "3. authorized keys for connecting with $PRIMARY_USER"
cat > /home/$PRIMARY_USER/.ssh/authorized_keys << EOF
# TODO
EOF
chmod 600 /home/$PRIMARY_USER/.ssh/authorized_keys

echo "4. private key for connecting to github/gitlab"
cat > /home/$PRIMARY_USER/.ssh/pi2_key_external << EOF
# TODO
EOF
chmod 600 /home/$PRIMARY_USER/.ssh/pi2_key_external

chown $PRIMARY_USER:$PRIMARY_USER -R /home/$PRIMARY_USER