#!/usr/bin/env bash
APP_NAME=$(jq '.app_name' etc/project_config.json | tr -d '"')
/var/www/${APP_NAME}/_manage.py "$@"
for i in /var/www/${APP_NAME}/store/migrations/versions/*.py; do
    sed -i 's/shipka.store.database.AwareDateTime(),/sa.DateTime(timezone=True),/' "$i";
    sed -i "s/op.drop_table('celery_taskmeta')// " "$i";
    sed -i "s/op.drop_table('celery_tasksetmeta')// " "$i";
    sed -i "s/sqlalchemy_utils.types.choice.ChoiceType(length=255), /sa.String(), / " "$i";
done
rm /var/www/${APP_NAME}/store/migrations/versions/*.pyc