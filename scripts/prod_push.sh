#!/usr/bin/env bash
shift
set -e

echo "*******************************************************************************"
echo "                       This will provision production!!!"
echo
echo "        MAKE SURE YOU HAVE THE CORRECT sensitive_config.json.prod FILE         "
echo
echo "*******************************************************************************"
echo
echo "First, it will install ansible on THIS machine. Then, make sure for production:"
echo "1. make sure user defined in hosts.ini exists and is in the sudoers file (e.g.: user ALL=(ALL) NOPASSWD: ALL)"
echo "   # useradd <user> && mkdir -p /home/<user>/.ssh"
echo "2. install openssh-server and python"
echo "   # apt install openssh-server python"
echo "3. add ssh key on prod machine - put public key in .ssh/authorized_keys"
echo
echo "Press Enter when ready and when sure. To cancel - Ctrl+C."
echo "*******************************************************************************"
read
if [ ! -f etc/sensitive_config.json.prod ]; then
    echo "        MAKE SURE YOU HAVE THE CORRECT sensitive_config.json.prod FILE         "
    exit 1
fi

# TODO
# useradd prodday_user && mkdir -p /home/prodday_user/.ssh
# apt update
# apt install openssh-server python
# apt install vim
# key.pub >> /home/prodday_user/.ssh/authorized_keys
# chown -R prodday_user:prodday_user /home/prodday_user

sudo pip install ansible==2.3.0.0
ansible-playbook -i hosts.ini /var/www/shipka/provisioning/playbook.yml --extra-vars "@etc/project_config.json" --extra-vars "@etc/sensitive_config.json.prod" --extra-vars "cwd=`pwd`" "$@"

REMOTE_CORE_PATH=`python -c 'import json;j=json.load(open("etc/project_config.json"));print(j["core_dir"])'`
APP_NAME=`python -c 'import json;j=json.load(open("etc/project_config.json"));print(j["app_name"])'`
PROJECT_ROOT="${REMOTE_CORE_PATH}/${APP_NAME}"

ansible-playbook -i hosts.ini "${PROJECT_ROOT}/provisioning/playbook.yml" --extra-vars "@etc/project_config.json" --extra-vars "@etc/sensitive_config.json.prod" --extra-vars "cwd=`pwd`" "$@"