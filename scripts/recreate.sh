#!/usr/bin/env bash
APP_NAME=`cat etc/project_config.json | jq -r '.app_name'`

echo ">>> Stop services..."
sudo systemctl stop eventlet pgweb celery beat

echo ">>> Stopping db connections..."
pkill python
sudo -u postgres psql --command "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = '${APP_NAME}' AND pid <> pg_backend_pid();"
echo

echo ">>> Dropping dbs..."
sudo -u postgres psql --command "drop database ${APP_NAME}_dev_db;"
sudo -u postgres psql --command "drop database ${APP_NAME}_test_db;"
echo

echo ">>> Creating dbs..."
sudo -u postgres psql --command "create database ${APP_NAME}_dev_db;"
sudo -u postgres psql --command "create database ${APP_NAME}_test_db;"
echo

echo ">>> Start services..."
sudo systemctl start eventlet pgweb celery beat
echo

echo ">>> Done!"