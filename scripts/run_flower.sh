#!/usr/bin/env bash
echo $$ > /var/run/flower.pid
source venv/bin/activate
flower "$@"