#!/usr/bin/env bash
sudo echo $$ > /var/run/eventlet.pid
source venv/bin/activate
./_manage.py runserver --host=127.0.0.1 --port=5000