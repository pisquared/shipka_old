#!/usr/bin/env python
import json
import os
import sys

project_path = sys.argv[1]
if not project_path:
    print("Project path needs to be specified as first argument. Exiting...")
    exit(1)

with open(os.path.join(project_path, 'etc', 'project_config.json')) as proj_f, open('templates/fe_assets.json.sample') as fe_assets_f:
    project_config = json.load(proj_f)
    fe_assets_config = json.load(fe_assets_f)

# load JQuery first
fe_assets_config['layout']['js'].append('client/js/vendor/jquery.js')

if project_config['main_css_framework'] == 'bootstrap':
    fe_assets_config['layout']['css'].append('client/css/vendor/bootstrap.css')
    fe_assets_config['layout']['js'].append('client/js/vendor/popper.js')
    fe_assets_config['layout']['js'].append('client/js/vendor/bootstrap.js')
elif project_config['main_css_framework'] == 'materialize':
    fe_assets_config['layout']['css'].append('client/css/vendor/materialize.css')

if project_config['font_awesome_enabled']:
    fe_assets_config['layout']['css'].append('client/fonts/font-awesome/css/font-awesome.min.css')

fe_assets_config['layout']['css'].append('client/css/global.css')


if project_config['websockets_enabled']:
    fe_assets_config['layout']['js'].append('client/js/vendor/socket.io.js')
    fe_assets_config['layout']['js'].append('client/js/shipka/socket_init.js')

fe_assets_config['layout']['js'].append('client/js/shipka/csrf.js')

if project_config['crudy_enabled']:
    fe_assets_config['layout']['js'].append('client/js/vendor/moment.js')
    fe_assets_config['layout']['js'].append('client/js/vendor/underscore.js')
    fe_assets_config['layout']['js'].append('client/js/vendor/backbone.js')
    fe_assets_config['layout']['js'].append('client/js/vendor/backbone.localStorage.js')
    fe_assets_config['layout']['js'].append('client/js/vendor/bootstrap-select.js')
    fe_assets_config['layout']['css'].append('client/css/vendor/bootstrap-select.css')
    fe_assets_config['layout']['js'].append('client/js/vendor/bootstrap-datetimepicker.js')
    fe_assets_config['layout']['css'].append('client/css/vendor/bootstrap-datetimepicker.css')
    fe_assets_config['layout']['js'].append('client/js/vendor/dragula.js')
    fe_assets_config['layout']['css'].append('client/css/vendor/dragula.css')
    fe_assets_config['layout']['js'].append('client/js/shipka/crudy.js')

fe_assets_config['layout']['js'].append('client/js/main.js')

with open('templates/fe_assets.tmp.json', 'w') as f:
    json.dump(fe_assets_config, f, indent=2)
