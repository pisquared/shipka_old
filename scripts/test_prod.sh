#!/usr/bin/env bash
shift
set -e

echo "*******************************************************************************"
echo "                       This will provision LOCAL TEST production!!!"
echo
echo "First, it will install ansible on THIS machine. Then it will bring up a vagrant test_prod."
echo
echo "Press Enter when ready and when sure. To cancel - Ctrl+C."
echo "*******************************************************************************"
read
sudo pip install ansible==2.3.0.0
vagrant up test_prod
vagrant provision --provision-with test_prod,test_prod_specific
