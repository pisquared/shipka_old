import sys

import os
from flask_script import Command, Manager, Option

from shipka.populate_command import PopulateCommand
from webapp import init_app, create_app

mode = 'development'
flask_webapp_config = os.environ.get('FLASK_WEBAPP_CONFIG', '')
if flask_webapp_config.lower() in ['prod', 'production']:
    mode = 'production'
    print
    print '*****************************'
    print '!!! RUNNING IN PRODUCTION !!!'
    print '*****************************'
    print

app = create_app()
if len(sys.argv) > 1 and sys.argv[1] != 'runserver':
    init_app(app, mode)


class Server(Command):
    option_list = (
        Option('--host', dest='host', default=None),
        Option('--port', dest='port', default=None),
        Option('--debug-fe', dest='debug_fe', default=False),
        Option('--debug-tb', dest='debug_tb_enabled', default=False),
        Option('--ssl', dest='ssl', default=False),
    )

    def _run_socketio(self, **kwargs):
        from webapp import socketio
        if kwargs.get('ssl'):
            # TODO: ssl
            socketio.run(app, host=kwargs.get('host'), port=int(kwargs.get('port')),
                         log_output=True,
                         keyfile='provisioning/dev_cert/localhost.key',
                         certfile='provisioning/dev_cert/localhost.crt', )
        else:
            socketio.run(app, host=kwargs.get('host'), port=int(kwargs.get('port')),
                         log_output=True)

    def run(self, **kwargs):
        init_app(app, mode, **kwargs)
        if not kwargs.get('host'):
            kwargs['host'] = app.config.get('APP_HOST')
        if not kwargs.get('port'):
            kwargs['port'] = int(app.config.get('APP_PORT'))
        if app.config.get('WEBSOCKETS_ENABLED'):
            self._run_socketio(**kwargs)
        else:
            app.run(host=kwargs.get('host'), port=int(kwargs.get('port')))


class AdminServer(Command):
    def run(self, **kwargs):
        from shipka.admin import app
        host = kwargs.get('host') or '0.0.0.0'
        port = kwargs.get('port') or 1337
        app.run(host=host, port=int(port))


class RunallCommand(Command):
    def run(self):
        from subprocess import list2cmdline
        from honcho.manager import Manager as HonchoManager

        os.environ['PYTHONUNBUFFERED'] = 'true'

        daemons = [
            ('server', ['./manage.sh', 'runserver']),
            ('worker', ['./manage.sh', 'celery']),
            ('cron', ['./manage.sh', 'beat']),
        ]

        cwd = os.path.abspath(os.path.dirname(__file__))
        honcho_manager = HonchoManager()
        for name, cmd in daemons:
            honcho_manager.add_process(
                name, list2cmdline(cmd),
                quiet=False, cwd=cwd,
            )

        honcho_manager.loop()
        sys.exit(honcho_manager.returncode)


manager = Manager(app)
manager.add_command("runall", RunallCommand())
manager.add_command("runserver", Server())
manager.add_command("runadmin", AdminServer())

# If we support databases, import the migration and populate commands
try:
    from flask_migrate import MigrateCommand

    manager.add_command('db', MigrateCommand)
    manager.add_command('populate', PopulateCommand())
except ImportError:
    print("INFO: Database import failed. It's OK if database not enabled.")
    pass

# If we support asset compilation, add to command
try:
    from flask_assets import ManageAssets

    manager.add_command("assets", ManageAssets())
except ImportError:
    print("INFO: Assets import failed. It's OK if assets not enabled.")
    pass

if __name__ == '__main__':
    manager.run()
