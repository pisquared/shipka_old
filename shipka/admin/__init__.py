import importlib
import inspect
import os

from flask import redirect
from flask_wtf.csrf import CSRFProtect

from shipka.config import Constants
from shipka.webapp import create_app, config_app, init_assets, init_common_components

app = create_app()
environment = os.environ.get('FLASK_WEBAPP_CONFIG') or 'development'

config_app(app, environment)

init_common_components(app, init_security=True)


def init_admin(app):
    CSRFProtect(app)
    try:
        init_assets(app)
    except Exception:
        pass
    try:
        from flask import abort, redirect, url_for, request
        from flask_admin import Admin, AdminIndexView, expose
        from flask_admin.contrib.sqla import ModelView
        from shipka.store.database import db, ModelController, User, Role, Log, LatestCUD
        from flask_security import login_required, roles_required, current_user
    except ImportError:
        raise ImportError('Flask-Admin is not installed')

    app.config['SECURITY_LOGIN_USER_TEMPLATE'] = 'admin/login.html'

    class AdminModelView(ModelView):
        def is_accessible(self):
            if not current_user.is_active or not current_user.is_authenticated:
                return False

            if current_user.has_role(Constants.ADMIN_ROLE):
                return True

            return False

        def _handle_view(self, name, **kwargs):
            """
            Override builtin _handle_view in order to redirect users when a view is not accessible.
            """
            if not self.is_accessible():
                if current_user.is_authenticated:
                    # permission denied
                    abort(403)
                else:
                    # login
                    return redirect(url_for('security.login', next=request.url))

    class MyHomeView(AdminIndexView):
        @expose('/')
        @login_required
        @roles_required(Constants.ADMIN_ROLE)
        def index(self):
            from shipka.store.database import User

            users = User.query.order_by(User.id).all()
            return self.render('admin/index.html', users=users)

    # admin = Admin(template_mode='bootstrap3', index_view=MyHomeView())
    admin = Admin(template_mode='bootstrap3', index_view=MyHomeView())
    admin.init_app(app)

    admin.add_view(AdminModelView(Role, db.session))
    admin.add_view(AdminModelView(User, db.session))
    admin.add_view(AdminModelView(Log, db.session))
    admin.add_view(AdminModelView(LatestCUD, db.session))

    # import subapps
    for _, dirs, _ in os.walk('webapp'):
        for dir in dirs:
            try:
                module = importlib.import_module("webapp.{}.models".format(dir))
            except ImportError:
                continue
            models = inspect.getmembers(module, inspect.isclass)
            for _, model_class in models:
                if issubclass(model_class, ModelController) and model_class is not ModelController:
                    try:
                        admin.add_view(AdminModelView(model_class, db.session))
                    except:
                        continue
        break


if app.config.get('DATABASE_ENABLED') and \
        app.config.get('AUTHENTICATION_ENABLED') and \
        app.config.get('ADMIN_WEBAPP_ENABLED'):
    init_admin(app)


    @app.route('/')
    def index():
        return redirect('/admin/')
