# -*- coding: utf-8 -*-
import json
import os

basedir = os.path.abspath(os.path.dirname(__file__))
project_config = json.load(open('etc/project_config.json'))


class Constants(object):
    MAX_FILE_SIZE = 5 * 1024 * 1024  # 5 MB
    ADMIN_ROLE = "admin"


class Config(object):
    def __init__(self, sensitive_config):
        self.APP_HOST = project_config.get("app_host")
        self.APP_PORT = project_config.get("app_port")

        self.ADMIN_WEBAPP_ENABLED = project_config.get("admin_webapp_enabled")
        self.WEBSOCKETS_ENABLED = project_config.get("websockets_enabled")
        self.WEBAPP_DEBUG_ENABLED = project_config.get("webapp_debug_enabled")

        self.ERROR_TRACKING_ENABLED = project_config.get("error_tracking_enabled")
        self.SENTRY_WEBAPP_TOKEN = sensitive_config.get("sentry_webapp_token")

        self.SECRET_KEY = "secretkey"
        self.WTF_CSRF_TIME_LIMIT = 15 * 60 * 60  # 15 min

        # Database Configuration
        self.DATABASE_ENABLED = project_config.get("database_enabled")
        self.PROPAGATE_EXCEPTIONS = True
        self.SQLALCHEMY_TRACK_MODIFICATIONS = True
        self.SQLALCHEMY_DATABASE_URI = 'postgresql://{user}:{password}@{socket}/{app_name}_dev_db'.format(
            app_name=project_config.get("app_name"),
            user=sensitive_config.get("db_user"),
            password=sensitive_config.get("db_password"),
            socket=sensitive_config.get("db_socket"))

        # Search configuration
        self.SEARCH_ENABLED = project_config.get("search_enabled")
        self.WHOOSHEE_DIR = os.path.join(project_config.get("core_dir"), project_config.get("app_name"), 'store',
                                         'search_index')

        # Cache configuration
        self.CACHE_ENABLED = project_config.get("cache_enabled")
        self.CACHE_TYPE = 'simple'
        self.CACHE_ACTIVATED = True  # change this disable caching

        # Broker configuration
        self.QUEUE_ENABLED = project_config.get("queue_enabled")
        self.WORKERS_ENABLED = project_config.get("workers_enabled")
        self.CELERY_RESULT_BACKEND = 'db+postgresql://{user}:{password}@{socket}/{app_name}_dev_db'.format(
            app_name=project_config.get("app_name"),
            user=sensitive_config.get("db_user"),
            password=sensitive_config.get("db_password"),
            socket=sensitive_config.get("db_socket"))
        self.CELERY_BROKER_URL = 'amqp://{user}:{password}@{socket}//'.format(
            user=sensitive_config.get("queue_user"),
            password=sensitive_config.get("queue_password"),
            socket=sensitive_config.get("queue_socket"))

        # Security (Login/Logout/Register) configurations
        self.AUTHENTICATION_ENABLED = project_config.get("authentication_enabled")
        self.SECURITY_PASSWORD_HASH = 'bcrypt'
        self.SECURITY_PASSWORD_SALT = "secretsalt"
        self.SECURITY_SEND_REGISTER_EMAIL = False
        self.SECURITY_CONFIRMABLE = False
        self.SECURITY_REGISTERABLE = True
        self.SECURITY_RECOVERABLE = False
        self.SECURITY_CHANGEABLE = True
        self.SECURITY_SEND_PASSWORD_CHANGE_EMAIL = False

        # Translations
        self.TRANSLATIONS_ENABLED = project_config.get('translations_enabled')
        self.TRANSLATION_LANGUAGES = project_config.get('translation_languages', [])
        self.BABEL_TRANSLATION_DIRECTORIES = os.path.join(project_config.get("core_dir"),
                                                          project_config.get("app_name"), 'translations')
        self.BABEL_DEFAULT_TIMEZONE = 'UTC'
        self.BABEL_DEFAULT_LOCALE = 'en'

        # Media uploads configurations
        self.UPLOADS_ENABLED = project_config.get('uploads_enabled')
        self.UPLOADS_DEFAULT_DEST = os.path.join(project_config.get("core_dir"),
                                                 project_config.get("app_name"), 'store', 'media')
        self.UPLOADS_DEFAULT_URL = '/media'

        # Gravatar settings
        self.GRAVATAR_ENABLED = project_config.get('gravatar_enabled')
        self.GRAVATAR_DEFAULT = 'mm'
        self.GRAVATAR_USE_SSL = True

        # Limiter settings
        self.LIMITER_ENABLED = project_config.get('limiter_enabled')
        self.RATELIMIT_DEFAULT = "90 per minute"

        # Markdown enabled
        self.MARKDOWN_ENABLED = project_config.get('markdown_enabled')

        # Should the app handle logging the requests straight into DB?
        # Pros:
        #   + Every request start/finish is in the db/Log, making it easier to get very verbose data
        # Cons:
        #   - Slower requests
        self.LOG_REQUESTS_INTO_DB = False


class ConfigDev(Config):
    def __init__(self, sensitive_config):
        super(ConfigDev, self).__init__(sensitive_config)
        self.MODE = 'development'

        self.DEBUG = True
        self.DEBUG_TB_ENABLED = False  # change this to enable/disable debug toolbar
        self.DEBUG_TB_INTERCEPT_REDIRECTS = False
        self.DEBUG_TB_PROFILER_ENABLED = False

        self.RATELIMIT_ENABLED = False


class ConfigTest(Config):
    def __init__(self, sensitive_config):
        super(ConfigTest, self).__init__(sensitive_config)
        self.MODE = 'testing'

        self.TESTING = True
        self.WTF_CSRF_ENABLED = False

        self.SQLALCHEMY_DATABASE_URI = 'postgresql://{user}:{password}@{socket}/{app_name}_test_db'.format(
            app_name=project_config.get("app_name"),
            user=sensitive_config.get("db_user"),
            password=sensitive_config.get("db_password"),
            socket=sensitive_config.get("db_socket"))

        self.CELERY_RESULT_BACKEND = 'db+postgresql://{user}:{password}@{socket}/{app_name}_test_db'.format(
            app_name=project_config.get("app_name"),
            user=sensitive_config.get("db_user"),
            password=sensitive_config.get("db_password"),
            socket=sensitive_config.get("db_socket"))


class ConfigProd(Config):
    def __init__(self, sensitive_config):
        super(ConfigProd, self).__init__(sensitive_config)
        self.MODE = 'production'

        self.SECRET_KEY = sensitive_config.get("secret_key")
        self.SECURITY_PASSWORD_SALT = sensitive_config.get("security_password_salt")

        self.SQLALCHEMY_DATABASE_URI = 'postgresql://{user}:{password}@{socket}/{app_name}_prod_db'.format(
            app_name=project_config.get("app_name"),
            user=sensitive_config.get("db_user"),
            password=sensitive_config.get("db_password"),
            socket=sensitive_config.get("db_socket"))

        self.CELERY_RESULT_BACKEND = 'db+postgresql://{user}:{password}@{socket}/{app_name}_prod_db'.format(
            app_name=project_config.get("app_name"),
            user=sensitive_config.get("db_user"),
            password=sensitive_config.get("db_password"),
            socket=sensitive_config.get("db_socket"))

        self.CELERY_BROKER_URL = 'amqp://{user}:{password}@{socket}//'.format(
            user=sensitive_config.get("queue_user"),
            password=sensitive_config.get("queue_password"),
            socket=sensitive_config.get("queue_socket"))


def init_config(env):
    assert env in ['dev', 'test', 'prod']
    if not os.path.isfile('etc/sensitive_config.json'):
        from shutil import copyfile

        copyfile('etc/sensitive_config.json.{}'.format(env), 'etc/sensitive_config.json')
    sensitive_config = json.load(open('etc/sensitive_config.json'))
    if env == 'dev':
        return ConfigDev(sensitive_config)
    elif env == 'test':
        return ConfigTest(sensitive_config)
    else:
        return ConfigProd(sensitive_config)


config_factory = {
    'test': init_config('test'),
    'testing': init_config('test'),
    'development': init_config('dev'),
    'dev': init_config('dev'),
    'production': init_config('prod'),
    'prod': init_config('prod'),
}
