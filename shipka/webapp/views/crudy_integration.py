from flask import render_template, request
from flask_login import current_user
from flask_socketio import join_room, emit
from shipka.store import database
from shipka.webapp import socketio
from shipka.webapp.util import get_latest, get_now_user

from shipka.webapp.registries import MODELS_REGISTRY

CRUDY_TEMPLATES = [
    'editable-input-body.html',
    'editable-input-title.html',
    'editable-input-str.html',
    'editable-input-int.html',
    'editable-select.html',
    'editable-dt.html',
    'flash.html',
]


def build_model_elements(form_include_map):
    elements = form_include_map
    for k, w in elements.iteritems():
        elements[k]['effective_type'] = w['type']
    if elements.get('title'):
        elements['title']['effective_type'] = 'title'
    if elements.get('body'):
        elements['body']['effective_type'] = 'body'
    if elements.get('name'):
        elements['name']['effective_type'] = 'title'
    return elements


def build_ctx():
    ctx = {
        'models': {
            model_view['vm'].name: {
                'name': model_view['vm'].name,
                'name_plural': model_view['vm'].name_plural,
                'name_human': model_view['vm'].name_human,
                'model_constants': model_view['vm'].model_constants,
                'elements': build_model_elements(model_view['vm'].form_include_map),
            } for model_view in MODELS_REGISTRY.values()
        },
        'crudy_templates': CRUDY_TEMPLATES
    }
    return ctx


@socketio.on('components_syn')
def socket_connect(json):
    payload = {}
    if current_user.is_authenticated:
        tz_offset_minutes = json.get('tz_offset_minutes')
        tz_offset_seconds = -(tz_offset_minutes * 60)
        if current_user.tz_offset_seconds != tz_offset_seconds:
            current_user.tz_offset_seconds = tz_offset_seconds
            database.add(current_user)
            database.push()
        model_updates = {}
        for model_name in MODELS_REGISTRY:
            model_updates[model_name] = get_latest(model_name)
        join_room(current_user.id)
        payload = {'user': {'id': current_user.id},
                   'request_sid': request.sid,
                   'datetime': str(get_now_user(current_user)),
                   'models_updates': model_updates}
    print('< components_syn: ' + str(json))
    print('> components_ack: ' + str(payload))
    emit('components_ack', payload)
