from flask import render_template, request
from flask_babel import refresh as refresh_lang

from shipka.store import database
from shipka.webapp.util import get_user
from shipka.webapp.views import app


@app.route('/translations')
def translations():
    user = get_user()
    user.timezone = request.args.get('timezone')
    user.locale = request.args.get('locale')
    database.add(user)
    database.push()
    refresh_lang()
    return render_template('dev/translations.html', user=user)
