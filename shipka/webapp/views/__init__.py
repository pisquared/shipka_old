from shipka.manager import app

from . import crudy_integration

if app.config.get('DEBUG'):
    from . import dev
    if app.config.get('WEBSOCKETS_ENABLED'):
        from . import dev_websocket
    if app.config.get('WORKERS_ENABLED'):
        from . import dev_worker
    if app.config.get('UPLOADS_ENABLED'):
        from . import dev_uploads
    if app.config.get('TRANSLATIONS_ENABLED'):
        from . import dev_translations
