from flask import render_template, request, flash

from shipka.webapp import media
from shipka.webapp.views import app


@app.route('/upload', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST' and 'media_file' in request.files:
        filename = media.save(request.files['media_file'])
        flash("File {} saved.".format(filename))
        return render_template('dev/upload.html')
    return render_template('dev/upload.html')
