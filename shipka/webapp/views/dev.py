from flask import render_template, redirect, request
from flask_login import login_required, logout_user, login_user
from flask_security import roles_required

from shipka.store.database import User
from shipka.webapp.util import get_user
from shipka.webapp.views import app


@app.route('/dev')
def dev():
    return render_template('dev/index.html', user=get_user())


@app.route('/admin-view-as')
@login_required
@roles_required('admin')
def admin_view_as():
    user_id = request.args.get('user_id')
    u = User.query.filter_by(id=user_id).first()
    if u:
        logout_user()
        login_user(u)
    return redirect('/')
