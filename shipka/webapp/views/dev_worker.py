from shipka.webapp.views import app
from shipka.workers import celery


@app.route('/tasks/sum/<int:x>/<int:y>')
def start_async_task(x, y):
    from shipka.workers.tasks import sum_args
    task_id = sum_args.delay(x, y)
    return "result will be available here <a href='/tasks/{task_id}/result'>{task_id}</a>".format(task_id=task_id)


@app.route('/tasks/<task_id>/result')
def get_async_task_result(task_id):
    result = celery.AsyncResult(id=task_id)
    return "Result: {result}".format(result=result.get())
