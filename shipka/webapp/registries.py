COMPONENT_REGISTRY = {}


def build_dependencies(model_scripts, view_components):
    view = {
        'components': [],
        'templates': [],
        'scripts': [],
        'external_scripts': [],
        'styles': [],
    }
    for view_component in view_components:
        component = COMPONENT_REGISTRY.get(view_component.component.name)
        component_name = component['name']
        app_name = component['app_name']
        view['components'].append(view_component)
        for component_key in ['templates', 'scripts', 'styles', 'external_scripts']:
            if component_key in component:
                for template in component[component_key]:
                    if (app_name, component_name, template) not in view[component_key]:
                        view[component_key].append((app_name, component_name, template))
    return model_scripts, view

MODELS_REGISTRY = {}


def update_model_registry(bp):
    # Populate MODELS_REGISTRY with extra metadata
    for model_name, registry in MODELS_REGISTRY.iteritems():
        MODELS_REGISTRY[model_name]['name'] = model_name
        VMClass = MODELS_REGISTRY[model_name]['vm']
        vm = VMClass(
            name=model_name,
            model=registry['model'],
            form_include=registry.get('form_include', []),
            form_exclude=registry.get('form_exclude', []),
            form_only=registry.get('form_only', []),
            creatable=registry.get('creatable', True),
            editable=registry.get('editable', True),
            deletable=registry.get('deletable', True),
            user_needed=registry.get('user_needed', True),
            creatable_login_required=registry.get('creatable_login_required', []),
            creatable_roles_required=registry.get('creatable_roles_required', []),
        )
        MODELS_REGISTRY[model_name]['vm'] = vm
        vm.register(bp)
