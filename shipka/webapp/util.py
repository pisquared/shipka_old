import datetime
import json
import pytz as pytz
import re

from collections import defaultdict
from dateutil import tz
from dateutil.parser import parse as dateparse
from flask import request, jsonify, render_template, current_app, redirect, session, flash
from flask_security import login_required, current_user, roles_required
from inflection import pluralize
from markupsafe import escape, Markup
from wtforms.fields.core import UnboundField, BooleanField, SelectField
from wtforms.fields.simple import TextAreaField
from wtforms_alchemy import ModelForm
from wtforms_components.fields.html5 import StringField, DateTimeField, IntegerField


class Proxy(object):
    def __init__(self):
        self.value = None

    def set(self, value):
        self.value = value

    def get(self):
        return self.value


global_user = Proxy()


def format_seconds_to_human(seconds):
    if not seconds:
        return "0 minutes"
    hours = seconds / 3600.0
    minutes = seconds / 60
    minutes_repr = '%d %s' % (minutes, "minute" if minutes == 1.0 else "minutes")
    if hours >= 1.0:
        hours_repr = '%d %s' % (int(hours), "hour" if int(hours) == 1 else "hours")
        minutes = (seconds - int(hours) * 3600) / 60.0
        minutes_repr = '%d %s' % (minutes, "minute" if minutes == 1.0 else "minutes")
        if minutes > 0.0:
            return '%s and %s' % (hours_repr, minutes_repr)
        return '%s' % hours_repr
    return minutes_repr


_paragraph_re = re.compile(r'(?:\r\n|\r|\n){2,}')


def create_jinja_helpers(app):
    @app.template_filter('seconds_to_human')
    def seconds_to_human(seconds):
        return format_seconds_to_human(seconds)

    @app.template_filter('float_to_tz')
    def float_to_tz(hours):
        int_hours = int(hours)
        minutes = int((hours - int_hours) * 60)
        return "{}{:02d}:{:02d}".format('+' if int_hours >= 0 else '-', int_hours, minutes)

    @app.template_filter('format_dt')
    def format_datetime(dt, formatting="%A, %d %b %Y"):
        """
        Formats the datetime string provided in value into whatever format you want that is supported by python strftime
        http://strftime.org/
        :param formatting The specific format of the datetime
        :param dt a datetime object
        :return:
        """
        return dt.strftime(formatting)

    @app.template_filter('nl2br')
    def nl2br(text):
        text = unicode(escape(text))
        result = u'<br>'.join(u'%s' % p.replace('\n', '<br>\n') for p in _paragraph_re.split(text))
        return Markup(result)

    @app.template_filter('hl_search')
    def highlight(text, query, n_hl=3):
        snippets = []
        for word in query.split():
            match_positions = [m.start() for m in re.finditer(word, text)]
            for mpos in match_positions:
                match_before = ' '.join(text[:mpos].split()[-3:])
                match = text[mpos:mpos + len(word)]
                match_after = ' '.join(text[mpos:].split()[1:4])
                snippet = "%s <b>%s</b> %s" % (escape(match_before), escape(match),
                                               escape(match_after))
                snippets.append(snippet)
        return '...'.join(snippets[:n_hl])


def register_request_hooks(app):
    # TODO: Exclude /static, /admin routes
    # TODO: Identify request_id and propagate it for the whole request timeframe
    def _log_request_start():
        from shipka.store.database import Log, add
        user = get_user()
        log = Log.create(created_ts=get_now_utc(),
                         action="REQUEST_START",
                         request_data={
                             'path': request.path
                         },
                         user=user)
        add(log)

    def _log_request_finish():
        from shipka.store.database import Log, add, push
        user = get_user()
        log = Log.create(created_ts=get_now_utc(),
                         action="REQUEST_FINISH",
                         user=user)
        add(log)
        push()

    @app.before_request
    def _app_before_request():
        if app.config["LOG_REQUESTS_INTO_DB"]:
            _log_request_start()

    @app.after_request
    def _app_after_request(response):
        if app.config["LOG_REQUESTS_INTO_DB"]:
            _log_request_finish()
        return response


def get_user(create=True):
    """
    Gets the currently saved user - either anonymous or authenticated.
    If no user is authenticated, create one with the next id and store
    it in the session.
    :return:
    """
    if global_user.get():
        return global_user.get()
    if current_user and current_user.is_authenticated:
        return current_user
    user_id = session.get('anonymous_user_id')
    from shipka.store.database import User, add, push
    if user_id:
        user = User.get_one_by(id=int(user_id))
        if user:
            return user
    # naively check if bot
    # user_agent = ua_parse(request.user_agent.string)
    # if user_agent.is_bot:
    #     return None
    if not create:
        return None
    user = User.create()
    add(user)
    push()
    session['anonymous_user_id'] = user.id
    return user


def get_now_utc(tzspecific=True):
    """
    Helper function to avoid all confusion about datetime.utcnow
    :param tzspecific: should we return Timezone specific datetime (usually yes)
    :return:
    """
    if tzspecific:
        return datetime.datetime.utcnow().replace(tzinfo=pytz.utc)
    return datetime.datetime.utcnow()


def get_user_tz(user):
    return tz.tzoffset(None, datetime.timedelta(seconds=user.tz_offset_seconds))


def get_now_user(user):
    """
    Gets the user specific time based on his timezone
    """
    tzinfo = get_user_tz(user)
    return datetime.datetime.utcnow().replace(tzinfo=tz.tzoffset(None, 0)).astimezone(tzinfo)


def get_today_user(user):
    """
    Gets the user specific beginning of today based on his timezone
    """
    now = get_now_user(user)
    return datetime.datetime(year=now.year, month=now.month, day=now.day, tzinfo=now.tzinfo)


def get_tomorrow_user(user):
    """
    Gets the user specific beginning of tomorrow based on his timezone
    """
    today = get_today_user(user)
    return today + datetime.timedelta(days=1)


def get_beginning_of_week_user(user):
    """
    Gets the user specific beginning of week based on his timezone
    """
    today = get_today_user(user)
    return today - datetime.timedelta(days=today.isoweekday() - 1)


def camel_case_to_snake_case(name):
    """
    Convertes a CamelCase name to snake_case
    :param name: the name to be converted
    :return:
    """
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


def snake_case_to_lower_camel_case(snake_str):
    components = snake_str.split('_')
    # We capitalize the first letter of each component except the first one
    # with the 'title' method and join them together.
    return components[0] + "".join(x.title() for x in components[1:])


def json_serialize(d):
    rv = {}
    for field, value in d.iteritems():
        try:
            if isinstance(value, datetime.datetime):
                value = value.strftime("%Y-%m-%d %H:%M:%S%Z")
            json.dumps(value)
            rv[field] = value
        except TypeError:
            continue
    return rv


def parse_dt_strategy(value, parser_f):
    dt = None
    if value:
        try:
            user = get_user()
            dt = parser_f(value).replace(tzinfo=get_user_tz(user))
        except TypeError, ValueError:
            dt = None
    return dt


def parse_dt(value):
    return parse_dt_strategy(value, dateparse)


def serialize_request_form(allowed_keys, payload):
    rv = {}
    for k, v in payload.iteritems():
        is_bool = k.endswith('_bool')
        if is_bool:
            k = k.split('_bool')[0]
            is_bool = True
        if k not in ['csrf_token', ] and k in allowed_keys:
            if type(v) in [unicode, str, ] and v.isdigit():
                v = int(v)
            if k.endswith('_ts'):
                v = parse_dt(v)
            if is_bool:
                v = bool(v)
            rv[k] = v
    return rv


class ModelView(object):
    def __init__(self,
                 name=None,
                 model=None,
                 form=None,
                 form_include=None,
                 form_exclude=None,
                 form_only=None,
                 retrievable_login_required=True,
                 retrievable_roles_required=None,
                 creatable=True,
                 creatable_login_required=True,
                 creatable_roles_required=None,
                 editable=True,
                 editable_login_required=True,
                 editable_roles_required=None,
                 deletable=True,
                 deletable_login_required=True,
                 deletable_roles_required=None,
                 user_needed=True,
                 ):
        form_include = form_include or []
        form_exclude = form_exclude or []
        form_only = form_only or []
        retrievable_roles_required = retrievable_roles_required or []
        creatable_roles_required = creatable_roles_required or []
        editable_roles_required = editable_roles_required or []
        deletable_roles_required = deletable_roles_required or []

        from shipka.store.database import LatestCUD
        self.LatestCUD = LatestCUD

        self.name = name
        self.name_human = name.replace('_', ' ')
        self.model = model
        self.model_constants = self._get_model_constants()
        self.form = form
        if not self.form:
            for ef in ['created_ts', 'updated_ts', 'deleted_ts', 'deleted']:
                if hasattr(self.model, ef):
                    form_exclude += [ef]
            self.form = self._create_form(form_include, form_exclude, form_only)
        self.form_include = self._get_form_include()
        self.form_include_map = self._get_form_include_map()
        self.name_plural = pluralize(self.name)

        self.retrievable_login_required = retrievable_login_required
        self.retrievable_roles_required = retrievable_roles_required

        self.creatable = creatable
        self.creatable_login_required = creatable_login_required
        self.creatable_roles_required = creatable_roles_required

        self.editable = editable
        self.editable_login_required = editable_login_required
        self.editable_roles_required = editable_roles_required

        self.deletable = deletable
        self.deletable_login_required = deletable_login_required
        self.deletable_roles_required = deletable_roles_required

        self.user_needed = user_needed

    def _create_form(self, form_include, form_exclude, form_only):
        """
        By default WTForms-Alchemy excludes a column from the ModelForm
        if one of the following conditions is True:
            Column is primary key
            Column is foreign key
            Column is DateTime field which has default value
                (usually this is a generated value)
            Column is of TSVectorType type
            Column is set as model inheritance discriminator field
        """

        return type('DynamicForm__{}'.format(self.name), (ModelForm,), {
            'Meta': type('Meta', (), {
                'model': self.model,
                'include': form_include,
                'exclude': form_exclude,
                'only': form_only,
            })
        })

    def _get_model_constants(self):
        return {c: getattr(self.model, c) for c in dir(self.model) if c.isupper()}

    def _get_form_include(self):
        form_include = [f for f in dir(self.form) if type(getattr(self.form, f)) is UnboundField]
        return ['request_sid'] + form_include

    def _get_form_include_map(self):
        """Maps fields to str representative type of the column"""
        rv = {}
        for f in dir(self.form):
            attr = getattr(self.form, f)
            if type(attr) is UnboundField:
                field_class = attr.field_class
                if issubclass(field_class, StringField):
                    rv[f] = {'type': 'str'}
                elif issubclass(field_class, DateTimeField):
                    rv[f] = {'type': 'dt'}
                elif issubclass(field_class, IntegerField):
                    rv[f] = {'type': 'int'}
                    if f.endswith('_id'):
                        connected_model = f.split('_id')[0]
                        rv[f]['type'] = 'select_model'
                        rv[f]['model'] = connected_model
                elif issubclass(field_class, BooleanField):
                    rv[f] = {'type': 'bool'}
                elif issubclass(field_class, TextAreaField):
                    rv[f] = {'type': 'str_long'}
                elif issubclass(field_class, SelectField):
                    rv[f] = {'type': 'select'}
                    rv[f]['choices'] = attr.kwargs['choices']
                else:
                    rv[f] = {'type': 'unknown', 'field_class': str(field_class)}
                rv[f]['default'] = attr.kwargs['default']
        return rv

    def init_form(self, **kwargs):
        user = get_user()
        form_instance = self.form(**kwargs)
        if hasattr(self.form, 'choice_methods'):
            for choice_field, meth in self.form.choice_methods.iteritems():
                if type(meth) is str and meth.startswith('user.'):
                    choices = [(a.id, a.name) for a in getattr(user, meth.split('user.')[1])]
                else:
                    choices = meth
                getattr(form_instance, choice_field).choices = choices
        return form_instance

    def add_create(self, form_data):
        from shipka.store import database
        from shipka.webapp import socketio
        user = get_user()
        now = get_now_user(user)
        latest_cud = self.LatestCUD.get_or_create(user_id=user.id, model_name=self.name)
        latest_cud.created_ts = get_now_user(user)
        request_sid = form_data.pop('request_sid', None)
        instance = self.model.create(created_ts=now, user=user, **form_data)
        database.add(instance)
        database.push()
        rv = {'instance': instance.serialize_flat(), 'request_sid': request_sid,
              'status': 'success'}
        if socketio:
            socketio.emit('{name}.created'.format(name=self.name), rv, room=user.id)
        return rv

    def add_update(self, instance, updated_elements):
        from shipka.store import database
        from shipka.webapp import socketio
        user = get_user()
        now = get_now_user(user)
        latest_cud = self.LatestCUD.get_or_create(user_id=user.id, model_name=self.name)
        latest_cud.updated_ts = now
        request_sid = updated_elements.pop('request_sid', None)
        print('updated_elements', updated_elements)
        database.update(instance, user_id=user.id, updated_ts=now, **updated_elements)
        database.push()
        rv = {'id': instance.id, 'updates': json_serialize(updated_elements),
              'status': 'success', 'request_sid': request_sid}
        if socketio:
            socketio.emit('{name}.updated'.format(name=self.name), rv, room=user.id)
        return rv

    def add_delete(self, instance):
        from shipka.store import database
        from shipka.webapp import socketio
        user = get_user()
        instance_id = instance.id
        latest_cud = self.LatestCUD.get_or_create(user_id=user.id, model_name=self.name)
        latest_cud.deleted_ts = get_now_user(user)
        database.delete(instance)
        database.push()
        request_sid = request.form.get('request_sid', None)
        rv = {'id': instance_id, 'request_sid': request_sid, 'status': 'success'}
        if socketio:
            socketio.emit('{name}.deleted'.format(name=self.name), rv, room=user.id)
        return rv

    def before_create(self, form_data):
        """override if needed - need to return form_data"""
        return form_data

    def after_create(self, serialized_instance):
        """override if needed - need to return serialized_instance"""
        return serialized_instance

    def handle_patch(self, instance, json):
        """override if needed - need to return updated elements"""
        request_sid = json.pop('request_sid', None)
        return {'request_sid': request_sid}

    def handle_get_filters(self, filters):
        """override if needed - need to filtered elements"""
        return self.default_get_all()

    def default_filters(self):
        filters = []
        if not self.user_needed:
            return []
        if hasattr(self.model, 'user'):
            user = get_user()
            filters = filters + [self.model.user == user]
        return filters

    def additional_filters(self):
        """override if needed"""
        return []

    def default_get_all(self):
        filters = self.default_filters()
        filters = filters + self.additional_filters()
        all = self.model.get_all_by(filters=filters)
        return all

    def default_get_one(self, instance_id):
        """override if needed - get one element by id"""
        if not self.user_needed:
            return self.model.get_one_by(id=instance_id)
        user = get_user()
        return self.model.get_one_by(id=instance_id, user=user)

    def serialize_batch_create(self):
        """override if needed - to create a batch creatable entity"""
        return request.form

    def serialize_batch_update(self):
        instances_updates = defaultdict(dict)
        for id_name in request.form.iterkeys():
            if id_name in ['csrf_token']:
                continue
            values = request.form.getlist(id_name)
            for value in values:
                splitted = id_name.split('_')
                instance_id, name = splitted[0], '_'.join(splitted[1:])
                if name.endswith('_bool') and name in instances_updates[instance_id]:
                    # In this case, the boolean flag is already in the custom form.
                    # That means that the second one is either the True of False but
                    # that means it is True if we are sending both (inputs checkboxes
                    # send only on checked=checked.
                    instances_updates[instance_id][name] = "1"
                    continue
                instances_updates[instance_id][name] = value
        return instances_updates

    def retrieve(self):
        with_ = request.args.getlist('with')
        if request.args:
            instances = self.handle_get_filters(request.args)
        else:
            instances = self.default_get_all()
        return jsonify([instance.serialize_flat(with_=with_) for instance in instances])

    def retrieve_single(self, instance_id):
        with_ = request.args.getlist('with')
        instance = self.default_get_one(instance_id)
        if not instance:
            error = 'No {name} with id {instance_id}'.format(name=self.name, instance_id=instance_id)
            return jsonify({"Error": error}), 404
        return jsonify(instance.serialize_flat(with_=with_))

    def create(self):
        payload = request.form or request.json
        include = self.form_include
        form_data = serialize_request_form(include, payload)
        form_instance = self.init_form(**form_data)
        if not form_instance.validate():
            return {"Error": form_instance.errors, 'status': 'error'}, 400
        try:
            form_data = self.before_create(form_data)
        except Exception as e:
            return {"Error": e.message, 'status': 'error'}, 400
        rv = self.add_create(form_data)
        rv = self.after_create(rv)
        return rv, 200

    def create_api(self):
        rv, status_code = self.create()
        return jsonify(rv), status_code

    def create_nojs(self):
        rv, _ = self.create()
        status = rv.get('status')
        next = request.form.get('next') or request.referrer
        if status == 'success':
            flash("Sucessfully created {}".format(self.name), 'success')
        elif status == 'error':
            flash("Error creating {}".format(self.name))
        return redirect(next)

    def after_batch_create(self, responses):
        """Override this for custom behaviour"""
        return responses

    def batch_create(self):
        rvs = []
        include = self.form_include
        custom_forms = self.serialize_batch_create()
        for custom_form in custom_forms:
            form_data = serialize_request_form(include, custom_form)
            form_instance = self.init_form(**form_data)
            if not form_instance.validate():
                return jsonify({"Error": form_instance.errors, 'status': 'error'}), 400
            try:
                form_data = self.before_create(form_data)
            except Exception as e:
                return jsonify({"Error": e.message, 'status': 'error'}), 400
            rvs.append(self.add_create(form_data))
        rvs = self.after_batch_create(rvs)
        return jsonify(rvs)

    def put(self, instance_id):
        """Used to update the whole element"""
        instance = self.model.get_one_by(id=instance_id)
        if not instance:
            error = 'No {name} with id {instance_id}'.format(name=self.name, instance_id=instance_id)
            return {"Error": error, 'status': 'error'}, 404
        payload = request.form or request.json
        include = self.form_include
        form_data = serialize_request_form(include, payload)
        form_instance = self.init_form(**form_data)
        if not form_instance.validate():
            return jsonify({"Error": form_instance.errors, 'status': 'error'}), 400
        rv = self.add_update(instance, form_data)
        return rv, 202

    def put_api(self, instance_id):
        rv, status_code = self.put(instance_id)
        return jsonify(rv), status_code

    def update_nojs(self, instance_id):
        rv, _ = self.put(instance_id)
        status = rv.get('status')
        next = request.form.get('next') or request.referrer
        if status == 'success':
            flash("Sucessfully updated {}".format(self.name), 'success')
        elif status == 'error':
            flash("Error updating {}".format(self.name))
        return redirect(next)

    def batch_update(self):
        rvs = []
        include = self.form_include
        instances_updates = self.serialize_batch_update()
        for instance_id, custom_form in instances_updates.iteritems():
            instance = self.model.get_one_by(id=instance_id)
            form_data = serialize_request_form(include, custom_form)
            form_instance = self.init_form(**form_data)
            if not form_instance.validate():
                return jsonify({"Error": form_instance.errors, 'status': 'error'}), 400
            rvs.append(self.add_update(instance, form_data))
        return jsonify(rvs)

    def patch(self, instance_id):
        """Used to (usually) update just a single item"""
        instance = self.model.get_one_by(id=instance_id)
        if not instance:
            error = 'No {name} with id {instance_id}'.format(name=self.name, instance_id=instance_id)
            return jsonify({"Error": error, 'status': 'error'}), 404
        payload = request.form or request.json
        try:
            updated_elements = self.handle_patch(instance, payload)
        except Exception as e:
            return jsonify({"Error": e.message}), 400
        rv = self.add_update(instance, updated_elements)
        return jsonify(rv)

    def delete(self, instance_id):
        instance = self.model.get_one_by(id=instance_id)
        if not instance:
            error = 'No {name} with id {instance_id}'.format(name=self.name, instance_id=instance_id)
            return {"Error": error, 'status': 'error'}, 404
        rv = self.add_delete(instance)
        return rv, 202

    def delete_api(self, instance_id):
        rv, status_code = self.delete(instance_id)
        return jsonify(rv), status_code

    def delete_nojs(self, instance_id):
        rv, _ = self.delete(instance_id)
        status = rv.get('status')
        next = request.form.get('next') or request.referrer
        if status == 'success':
            flash("Sucessfully deleted {}".format(self.name), 'success')
        elif status == 'error':
            flash("Error deleting {}".format(self.name))
        return redirect(next)

    def _register(self):
        """Override this to register more views"""

    def decorate_view_func(self, view_func, is_login, are_roles):
        if is_login:
            view_func = login_required(view_func)
        if are_roles:
            view_func = roles_required(*are_roles)(view_func)
        return view_func

    def _def_create_routes(self, blueprint):
        creatable_view_func = self.create_api
        creatable_view_func = self.decorate_view_func(creatable_view_func,
                                                      self.creatable_login_required,
                                                      self.creatable_roles_required)
        blueprint.add_url_rule('/api/{name_plural}'.format(name_plural=self.name_plural),
                               endpoint='{name}_create'.format(name=self.name),
                               view_func=creatable_view_func,
                               methods=['POST'])
        creatable_view_func_no_js = self.create_nojs
        creatable_view_func_no_js = self.decorate_view_func(creatable_view_func_no_js,
                                                            self.creatable_login_required,
                                                            self.creatable_roles_required)
        blueprint.add_url_rule('/api/{name_plural}/create'.format(name_plural=self.name_plural),
                               endpoint='{name}_create_nojs'.format(name=self.name),
                               view_func=creatable_view_func_no_js,
                               methods=['POST'])
        batch_create_view_func = self.batch_create
        batch_create_view_func = self.decorate_view_func(batch_create_view_func,
                                                         self.creatable_login_required,
                                                         self.creatable_roles_required)
        blueprint.add_url_rule('/api/{name_plural}/batch_create'.format(name_plural=self.name_plural),
                               endpoint='{name}_batch_create'.format(name=self.name),
                               view_func=batch_create_view_func,
                               methods=["POST"])

    def _def_retrieve_routes(self, blueprint):
        retrieve_view_func = self.retrieve
        retrieve_view_func = self.decorate_view_func(retrieve_view_func,
                                                     self.retrievable_login_required,
                                                     self.retrievable_roles_required)
        blueprint.add_url_rule('/api/{name_plural}'.format(name_plural=self.name_plural),
                               endpoint='{name}_retrieve_all'.format(name=self.name),
                               view_func=retrieve_view_func,
                               methods=['GET'])
        retrieve_single_view_func = self.retrieve_single
        retrieve_single_view_func = self.decorate_view_func(retrieve_single_view_func,
                                                            self.retrievable_login_required,
                                                            self.retrievable_roles_required)
        blueprint.add_url_rule('/api/{name_plural}/<int:instance_id>'.format(name_plural=self.name_plural),
                               endpoint='{name}_retrieve_single'.format(name=self.name),
                               view_func=retrieve_single_view_func,
                               methods=['GET'])

    def _def_update_routes(self, blueprint):
        patch_view_func = self.patch
        patch_view_func = self.decorate_view_func(patch_view_func,
                                                  self.editable_login_required,
                                                  self.editable_roles_required)
        blueprint.add_url_rule('/api/{name_plural}/<int:instance_id>'.format(name_plural=self.name_plural),
                               endpoint='{name}_patch'.format(name=self.name),
                               view_func=patch_view_func,
                               methods=['PATCH'])
        put_api_view_func = self.put_api
        put_api_view_func = self.decorate_view_func(put_api_view_func,
                                                    self.editable_login_required,
                                                    self.editable_roles_required)
        blueprint.add_url_rule('/api/{name_plural}/<int:instance_id>'.format(name_plural=self.name_plural),
                               endpoint='{name}_put'.format(name=self.name),
                               view_func=put_api_view_func,
                               methods=['PUT'])
        update_nojs_view_func = self.update_nojs
        update_nojs_view_func = self.decorate_view_func(update_nojs_view_func,
                                                        self.editable_login_required,
                                                        self.editable_roles_required)
        blueprint.add_url_rule('/api/{name_plural}/<int:instance_id>'.format(name_plural=self.name_plural),
                               endpoint='{name}_update_nojs'.format(name=self.name),
                               view_func=update_nojs_view_func,
                               methods=['POST'])
        batch_update_view_func = self.batch_update
        batch_update_view_func = self.decorate_view_func(batch_update_view_func,
                                                         self.editable_login_required,
                                                         self.editable_roles_required)
        blueprint.add_url_rule('/api/{name_plural}/batch_update'.format(name_plural=self.name_plural),
                               endpoint='{name}_batch_update'.format(name=self.name),
                               view_func=batch_update_view_func,
                               methods=["POST"])

    def _def_delete_routes(self, blueprint):
        delete_api_view_func = self.delete_api
        delete_api_view_func = self.decorate_view_func(delete_api_view_func,
                                                       self.deletable_login_required,
                                                       self.deletable_roles_required)
        blueprint.add_url_rule('/api/{name_plural}/<int:instance_id>'.format(name_plural=self.name_plural),
                               endpoint='{name}_delete'.format(name=self.name),
                               view_func=delete_api_view_func,
                               methods=['DELETE'])
        delete_nojs_view_func = self.delete_nojs
        delete_nojs_view_func = self.decorate_view_func(delete_nojs_view_func,
                                                        self.deletable_login_required,
                                                        self.deletable_roles_required)
        blueprint.add_url_rule('/api/{name_plural}/<int:instance_id>/delete'.format(name_plural=self.name_plural),
                               endpoint='{name}_delete_nojs'.format(name=self.name),
                               view_func=delete_nojs_view_func,
                               methods=['GET', 'POST'])

    def register(self, blueprint):
        self._def_retrieve_routes(blueprint)
        if self.creatable:
            self._def_create_routes(blueprint)
        if self.editable:
            self._def_update_routes(blueprint)
        if self.deletable:
            self._def_delete_routes(blueprint)
        self._register()


def unix_ts(dt):
    if not dt:
        return 0
    utc_naive = dt.replace(tzinfo=None) - dt.utcoffset()
    return (utc_naive - datetime.datetime(1970, 1, 1)).total_seconds()


def get_latest(model_name):
    from shipka.store.database import LatestCUD
    created, updated, deleted = 0, 0, 0
    user = get_user()
    latest_cud = LatestCUD.get_one_by(user=user, model_name=model_name)
    if latest_cud:
        created = unix_ts(latest_cud.created_ts)
        updated = unix_ts(latest_cud.updated_ts)
        deleted = unix_ts(latest_cud.deleted_ts)
    latest = max(created, updated, deleted)
    return {
        'model_name': model_name,
        'latest': latest,
        'created': created,
        'updated': updated,
        'deleted': deleted,
    }


def send_gcm_notification(notification):
    for gcm_token in notification.user.gcm_tokens:
        ctx = json.loads(notification.msg_context or '{}')
        ctx["type"] = notification.notification_type
        payload = {
            "data": ctx,
            "to": gcm_token.token
        }
        # TODO: GCM Requests
        # requests.post("https://gcm-http.googleapis.com/gcm/send",
        #               headers={"Content-Type": "application/json",
        #                        "Authorization": "key=" + current_app.config.get('GCM_API_KEY')},
        #               data=json.dumps(payload))
