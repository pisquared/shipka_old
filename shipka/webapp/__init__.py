import importlib
import json

import os
from flask import Flask, render_template, request, g
from flask_assets import Environment, Bundle
from flask_compress import Compress
from flask_wtf.csrf import CSRFProtect

from shipka.config import config_factory, Constants

assets = Environment()
csrf = CSRFProtect()
compress = Compress()
db = None
limiter = None
socketio = None
sentry = None
media = None
gravatar = None


def _init_translations(app):
    from flask_babel import Babel
    babel = Babel(app)

    @babel.localeselector
    def get_locale():
        # if a user is logged in, use the locale from the user settings
        from shipka.webapp.util import get_user
        user = get_user(create=False)
        if user is not None:
            return user.locale
        # otherwise try to guess the language from the user accept
        # header the browser transmits
        return request.accept_languages.best_match(app.config.get('TRANSLATION_LANGUAGES', ['en']))

    @babel.timezoneselector
    def get_timezone():
        from shipka.webapp.util import get_user
        user = get_user()
        if user is not None:
            return user.timezone


def config_assets(app):
    with open(os.path.join(app.config['core_dir'], app.config['app_name'],
                           'webapp', 'fe_assets.json')) as fe_assets_f:
        fe_assets_config = json.load(fe_assets_f)

    for key in fe_assets_config:
        app.config['css_{}_gen'.format(key)] = 'gen/{}.min.css'.format(key)
        app.config['css_{}_elements'.format(key)] = fe_assets_config[key]['css']

        app.config['js_{}_gen'.format(key)] = 'gen/{}.min.js'.format(key)
        app.config['js_{}_elements'.format(key)] = fe_assets_config[key]['js']


def init_assets(app):
    config_assets(app)
    assets.init_app(app)
    with app.app_context():
        assets.directory = os.path.join(app.config['core_dir'], app.config['app_name'], 'store', 'static')

    # TODO: DRY with above - reading this file twice
    with open(os.path.join(app.config['core_dir'], app.config['app_name'],
                           'webapp', 'fe_assets.json')) as fe_assets_f:
        fe_assets_config = json.load(fe_assets_f)

    for key in fe_assets_config:
        css_layout = Bundle(*app.config['css_{}_elements'.format(key)],
                            filters='cssmin', output=app.config['css_{}_gen'.format(key)])
        assets.register('css_{}'.format(key), css_layout)

        js_layout = Bundle(*app.config['js_{}_elements'.format(key)],
                           filters='jsmin', output=app.config['js_{}_gen'.format(key)])
        assets.register('js_{}'.format(key), js_layout)


def _init_security(app, db):
    from flask_security import Security, SQLAlchemyUserDatastore, login_required, logout_user
    from shipka.store.database import User, Role
    from shipka.webapp.forms import ExtendedRegisterForm
    security = Security()

    user_datastore = SQLAlchemyUserDatastore(db, User, Role)
    security.init_app(app, user_datastore, register_form=ExtendedRegisterForm)

    @login_required
    @app.route("/log_out")
    def logout():
        logout_user()
        return render_template("logout.html")


def _init_cached_login_mgr(app, cache):
    from flask_sqlalchemy_cache import FromCache
    from shipka.store.database import User

    @app.login_manager.user_loader
    def _load_user(id=None):
        return User.query.options(FromCache(cache)).filter_by(id=id).first()


def _init_db(app):
    from flask_migrate import Migrate
    from shipka.store.database import db
    db.init_app(app)
    migrate = Migrate()
    migrate.init_app(app, db, directory='store/migrations')
    migrate.init_app(app, db)
    return db


def _init_search(app):
    from shipka.store.search import whooshee
    whooshee.init_app(app)
    return whooshee


def _init_debug_toolbar(app):
    from flask_debugtoolbar import DebugToolbarExtension
    debug_toolbar = DebugToolbarExtension()
    debug_toolbar.init_app(app)


def _init_markdown(app):
    from flaskext.markdown import Markdown
    Markdown(app, auto_escape=True)


def _init_uploads(app):
    global media
    from flask_uploads import patch_request_class, UploadSet, configure_uploads
    media = UploadSet('media')
    patch_request_class(app, Constants.MAX_FILE_SIZE)
    configure_uploads(app, (media,))


def _init_cache(app):
    from flask_cache import Cache
    cache = Cache()
    return cache


def _init_gravatar(app):
    global gravatar
    from flask_gravatar import Gravatar
    gravatar = Gravatar(app)


def _init_limiter(app):
    global limiter
    from flask_limiter import Limiter
    from flask_limiter.util import get_remote_address
    limiter = Limiter(app, key_func=get_remote_address)


def register_app_handlers(app):
    def _construct_error_ctx():
        from shipka.webapp.util import get_user
        ctx = dict(user=get_user())
        if sentry and g.mode == assets.AppModes.PRODUCTION:
            ctx["event_id"] = g.sentry_event_id
            ctx["public_dsn"] = sentry.client.get_public_dsn('https')
        return ctx

    if app.config.get('MODE') in ['prod', 'production']:
        @app.errorhandler(Exception)
        @app.errorhandler(500)
        def _internal_server_error(error):
            ctx = _construct_error_ctx()
            return render_template('errors/500.html', **ctx), 500

        @app.errorhandler(404)
        def _not_found(error):
            ctx = _construct_error_ctx()
            return render_template('errors/404.html', **ctx), 404


def create_app():
    return Flask(__name__,
                 static_folder=os.path.join(os.getcwd(), 'store', 'static'))


def config_app(app, option, **kwargs):
    assert option in config_factory.keys()
    config = config_factory.get(option)
    for config_file in ['project_config.json', 'app_sensitive.json']:
        project_config = json.load(open(os.path.join(os.getcwd(), 'etc', config_file)))
        app.config.from_object(config)
        updated_config = {k.upper(): v for k, v in kwargs.iteritems()}
        updated_config.update(project_config)
        app.config.update(updated_config)


def init_common_components(app, init_security=False):
    if app.config.get('WEBSOCKETS_ENABLED'):
        global socketio
        from flask_socketio import SocketIO
        if app.config.get('QUEUE_ENABLED'):
            socketio = SocketIO(app, message_queue=app.config.get('CELERY_BROKER_URL'))
        else:
            socketio = SocketIO(app)

    if app.config.get('DATABASE_ENABLED'):
        global db
        db = _init_db(app)
        if app.config.get('SEARCH_ENABLED'):
            _init_search(app)
            if init_security:
                _init_security(app, db)


def import_product_webapp(app):
    for _, dirs, _ in os.walk('webapp'):
        for dir in dirs:
            with app.app_context():
                module = importlib.import_module("webapp.{}".format(dir))
            attr = getattr(module, dir)
            app.register_blueprint(attr)
        break


def init_app(app, option, **kwargs):
    config_app(app, option, **kwargs)

    init_assets(app)
    csrf.init_app(app)
    compress.init_app(app)

    init_common_components(app)

    if app.config.get('TRANSLATIONS_ENABLED'):
        _init_translations(app)

    if app.config.get('WEBAPP_DEBUG_ENABLED') and app.config.get('DEBUG'):
        _init_debug_toolbar(app)

    if app.config.get('MARKDOWN_ENABLED'):
        _init_markdown(app)

    if app.config.get('GRAVATAR_ENABLED'):
        _init_gravatar(app)

    if app.config.get('LIMITER_ENABLED'):
        _init_limiter(app)

    if app.config.get('ERROR_TRACKING_ENABLED'):
        global sentry
        from raven.contrib.flask import Sentry
        sentry = Sentry(app, dsn='http://{sentry_token}@localhost:9000/2'.format(
            sentry_token=app.config.get('SENTRY_WEBAPP_TOKEN')))

    register_app_handlers(app)

    # patch max request size
    if app.config.get('UPLOADS_ENABLED'):
        _init_uploads(app)

    # import blueprints
    from shipka.webapp import views
    with app.app_context():
        from webapp.client import client
    app.register_blueprint(client)

    # importing here before security init so that security template folder is overriden in the product app
    import_product_webapp(app)
    if app.config.get('AUTHENTICATION_ENABLED'):
        _init_security(app, db)

    if app.config.get('CACHE_ENABLED'):
        cache = _init_cache(app)
        if option == 'production' or app.config['CACHE_ACTIVATED']:
            cache.init_app(app)
            if app.config.get('AUTHENTICATION_ENABLED'):
                _init_cached_login_mgr(app, cache)

    from shipka.webapp.util import create_jinja_helpers, register_request_hooks
    create_jinja_helpers(app)
    register_request_hooks(app)

    return app
