import json
import time

from flask_script import Command


class PopulateCommand(Command):
    def _create_admin(self, email, password):
        from shipka.store.database import Role, User, add

        try:
            from flask_security.utils import hash_password
        except ImportError:
            raise ImportError("Flask-Security is not installed!")

        # create admin role
        admin_role = Role.get_or_create(name='admin')
        add(admin_role)

        admin_user = User.get_one_by(email=email)

        if not admin_user:
            admin_user = User.create(email=email,
                                     name=u"Admin",
                                     password=hash_password(password),
                                     tz_offset_seconds=-time.timezone)
            admin_user.roles = [admin_role]
            add(admin_user)

    def run(self):
        from shipka.store import database
        from shipka.manager import app
        with app.app_context():
            if app.config.get('ADMIN_WEBAPP_ENABLED'):
                sensitive_config = json.load(open('etc/sensitive_config.json'))
                self._create_admin(email=sensitive_config.get("admin_webapp_user"),
                                   password=sensitive_config.get('admin_webapp_password'))

            from populate import populate
            populate()
            database.push()
