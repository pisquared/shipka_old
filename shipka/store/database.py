"""
This module provides communication with local database.
"""
import datetime
import json
from json import JSONEncoder

from dateutil import tz
from flask_login import current_user
from flask_security import RoleMixin, UserMixin
from flask_sqlalchemy import SQLAlchemy, Model
from sqlalchemy.ext.declarative import DeclarativeMeta
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm.collections import InstrumentedList
from sqlalchemy_utils import Choice, ChoiceType

from shipka.webapp.util import get_now_utc, camel_case_to_snake_case, get_user

try:
    from flask_sqlalchemy_cache import CachingQuery

    Model.query_class = CachingQuery
    db = SQLAlchemy(query_class=CachingQuery)
except ImportError:
    # This would fail if database caching is not enabled but it's too early to check for that
    print("INFO: Database caching failed. It's OK if database-cache not enabled.")
    db = SQLAlchemy()


def create_db():
    db.create_all()


def drop_db():
    db.session.remove()
    db.drop_all()


def update(model, **kwargs):
    for k, w in kwargs.iteritems():
        setattr(model, k, w)
    db.session.add(model)
    return model


def add(instance):
    db.session.add(instance)


def delete(instance):
    now = get_now_utc()
    if hasattr(instance, 'deleted'):
        instance.deleted = True
        instance.deleted_ts = now
        db.session.add(instance)
    else:
        db.session.delete(instance)


def push():
    db.session.commit()


class ModelJsonEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime.datetime):
            return str(o)
        return o.id


class ModelController(ModelJsonEncoder):
    """
    This interface is the parent of all models in our database.
    """

    @declared_attr
    def __tablename__(self):
        return camel_case_to_snake_case(self.__name__)  # pylint: disable=E1101

    _sa_declared_attr_reg = {'__tablename__': True}
    __mapper_args__ = {'always_refresh': True}

    id = db.Column(db.Integer, primary_key=True)

    _excluded_serialization = []

    @classmethod
    def create(cls, **kwargs):
        if issubclass(cls, Datable):
            kwargs['created_ts'] = get_now_utc()
        instance = cls(**kwargs)
        add(instance)

        model = cls.__name__
        if model in ['Log']:
            return instance
        user = get_user(create=False)
        log = Log.create(created_ts=get_now_utc(),
                         action="MODEL_CREATED",
                         model=model,
                         model_data=json.dumps(kwargs, cls=ModelJsonEncoder),
                         user=user)
        add(log)
        return instance

    @classmethod
    def _filter_method(cls, filters=None, **kwargs):
        if filters is None:
            filters = []
        if hasattr(cls, "deleted"):
            kwargs['deleted'] = kwargs.get('deleted', False)

        order_by = kwargs.get("order_by", None)
        if order_by is not None:
            del kwargs['order_by']

        # import pdb; pdb.set_trace();
        rv_method = filterings = cls.query.filter_by(**kwargs).filter(*filters)

        if order_by is not None:
            rv_method = filterings.order_by(order_by)
        return rv_method

    @classmethod
    def get_by(cls, first=0, last=None, filters=None, **kwargs):
        return cls._filter_method(filters, **kwargs).all()[first:last]

    @classmethod
    def get_one_by(cls, filters=None, **kwargs):
        return cls._filter_method(filters, **kwargs).first()

    @classmethod
    def get_all_by(cls, filters=None, **kwargs):
        return cls._filter_method(filters, **kwargs).all()

    @classmethod
    def get_all(cls, **kwargs):
        return cls.query.all()

    @classmethod
    def get_or_create(cls, **kwargs):
        instance = cls.get_one_by(**kwargs)
        if not instance:
            instance = cls.create(**kwargs)
        return instance

    def serialize_flat(self, with_=None, depth=0, withs_used=None):
        """
        Serializes object to dict
        It will ignore fields that are not encodable (set them to 'None').

        It expands relations mentioned in with_ recursively up to MAX_DEPTH and tries to smartly ignore
        recursions by mentioning which with elements have already been used in previous depths
        :return:
        """

        MAX_DEPTH = 3

        def with_used_in_prev_depth(field, previous_depths):
            for previous_depth in previous_depths:
                if field in previous_depth:
                    return True
            return False

        def handle_withs(data, with_, depth, withs_used):
            if isinstance(data, InstrumentedList):
                if depth >= MAX_DEPTH:
                    return [e.serialize_flat() for e in data]
                else:
                    return [e.serialize_flat(with_=with_, depth=depth + 1, withs_used=withs_used) for e in data]
            else:
                if depth >= MAX_DEPTH:
                    return data.serialize_flat()
                else:
                    return data.serialize_flat(with_=with_, depth=depth + 1, withs_used=withs_used)

        if not with_:
            with_ = []
        if not withs_used:
            withs_used = []
        if isinstance(self.__class__, DeclarativeMeta):
            # an SQLAlchemy class
            fields = {}
            iterable_fields = [x for x in dir(self) if not x.startswith('_') and x not in ['metadata',
                                                                                           'item_separator',
                                                                                           'key_separator'] and x.islower()
                               and x not in self._excluded_serialization]
            for field in iterable_fields:
                data = self.__getattribute__(field)
                try:
                    if field in with_:
                        # this hanldes withs nested inside other models
                        if len(withs_used) < depth + 1:
                            withs_used.append([])
                        previous_depths = withs_used[:depth]
                        if with_used_in_prev_depth(field, previous_depths):
                            continue
                        withs_used[depth].append(field)
                        data = handle_withs(data, with_, depth, withs_used)
                    if isinstance(data, datetime.datetime):
                        data = str(data)
                    if isinstance(data, Choice):
                        data = data.code
                    json.dumps(data)  # this will fail on non-encodable values, like other classes
                    if isinstance(data, InstrumentedList):
                        continue  # pragma: no cover
                    fields[field] = data
                except TypeError:
                    pass  # Don't assign anything
            # a json-encodable dict
            return fields


class AwareDateTime(db.TypeDecorator):
    """
    Results returned as aware datetimes, not naive ones.
    """

    impl = db.DateTime

    def process_result_value(self, value, dialect):
        if current_user and current_user.is_authenticated and value:
            tzinfo = tz.tzoffset(None, datetime.timedelta(seconds=current_user.tz_offset_seconds))
            return value.astimezone(tzinfo)
        return value


class Deletable(object):
    """
    This interface adds deletable properties to a model.
    """
    deleted = db.Column(db.Boolean, default=False)
    deleted_ts = db.Column(AwareDateTime())

    def undelete(self):
        self.deleted = False
        self.deleted_ts = None
        db.session.add(self)


class Datable(object):
    created_ts = db.Column(AwareDateTime())
    updated_ts = db.Column(AwareDateTime())


class Ownable(object):
    @declared_attr
    def user_id(self):
        return db.Column(db.Integer, db.ForeignKey('user.id'))

    @declared_attr
    def user(self):
        return db.relationship("User")


roles_permissions = db.Table('roles_permissions',
                             db.Column('permission_id', db.Integer(), db.ForeignKey('permission.id')),
                             db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))

roles_users = db.Table('roles_users',
                       db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
                       db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))


class Permission(db.Model, ModelController, RoleMixin):
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

    def __repr__(self):
        return '<Permission %r>' % self.name


class Role(db.Model, ModelController, RoleMixin):
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

    def __repr__(self):
        return '<Role %r>' % self.name


class User(db.Model, ModelController, UserMixin, Deletable):
    _excluded_serialization = ['password']

    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))

    name = db.Column(db.Unicode)
    image_url = db.Column(db.String)

    timezone = db.Column(db.String, default='UTC')
    tz_offset_seconds = db.Column(db.Integer, default=0)
    locale = db.Column(db.String(4), default='en')

    active = db.Column(db.Boolean(), default=True)
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))

    @classmethod
    def create(cls, **kwargs):
        instance = super(User, cls).create(**kwargs)
        add(instance)
        push()
        return instance

    def has_role(self, role):
        return role in self.roles

    def __repr__(self):
        return '<User %r>' % self.email


class Log(db.Model, ModelController):
    """Logs all events happened on our end with somewhat structured data"""
    created_ts = db.Column(db.DateTime())  # UTC Datetime of this event

    action = db.Column(db.String(20))  # MODEL_[CREATED|UPDATED|DELETED], TASK_[CREATED|STARTED|FINISHED]
    level = db.Column(db.String(20), default="INFO")  # INFO|WARN|ERROR

    request_id = db.Column(db.Integer)  # if possible track as a part of request_id
    request_data = db.Column(db.JSON)  # serialized request data

    model = db.Column(db.String(255))  # the name of the model about which is this log entry
    model_data = db.Column(db.JSON)  # serialized model data

    task_data = db.Column(db.JSON)  # serialized task data

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship("User", backref='logs')


class LatestCUD(db.Model, ModelController, Datable, Deletable):
    model_name = db.Column(db.String(80))

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship("User", backref='latest_cuds')


class App(db.Model, ModelController):
    name = db.Column(db.String)
    description = db.Column(db.String)
    developer = db.Column(db.String)
    icon = db.Column(db.String)

    def __repr__(self):
        return '<App %r - %r>' % (self.id, self.name)


class View(db.Model, ModelController, Ownable, Datable):
    name = db.Column(db.Unicode)
    route = db.Column(db.Unicode)
    layout = db.Column(db.Unicode)

    options = db.Column(db.Unicode)

    filters = db.Column(db.Unicode)


class Component(db.Model, ModelController):
    name = db.Column(db.String)
    collection = db.Column(db.String)
    type = db.Column(db.String, default="list")

    layout = db.Column(db.Unicode)
    options = db.Column(db.Unicode)

    filters = db.Column(db.Unicode)

    def __repr__(self):
        return '<Component %r - %r>' % (self.id, self.name)


class AppView(db.Model, ModelController, Datable):
    name = db.Column(db.Unicode)

    layout = db.Column(db.Unicode)

    options = db.Column(db.Unicode)

    filters = db.Column(db.Unicode)

    app_id = db.Column(db.Integer, db.ForeignKey('app.id'))
    app = db.relationship("App", backref="views")


class AppComponent(db.Model, ModelController, Datable):
    name = db.Column(db.Unicode)
    template = db.Column(db.Unicode)

    layout = db.Column(db.Unicode)
    options = db.Column(db.Unicode)

    filters = db.Column(db.Unicode)

    app_id = db.Column(db.Integer, db.ForeignKey('app.id'))
    app = db.relationship("App", backref="components")


############ PER USER

class Contextable(object):
    @declared_attr
    def context_id(self):
        return db.Column(db.Integer, db.ForeignKey('context.id'))

    @declared_attr
    def context(self):
        return db.relationship("Context")


class UserInstalledApp(db.Model, ModelController, Ownable, Datable):
    app_id = db.Column(db.Integer, db.ForeignKey('app.id'))
    app = db.relationship("App", backref='user_apps')

    enabled = db.Column(db.Boolean, default=False)


class Context(db.Model, ModelController, Ownable, Datable):
    name = db.Column(db.Unicode)

    filters = db.Column(db.Unicode)


class UserView(db.Model, ModelController, Ownable, Contextable):
    name = db.Column(db.String)
    icon = db.Column(db.String)

    on_main = db.Column(db.Boolean, default=True)  # should it be on the main menu
    order = db.Column(db.Integer)

    def __repr__(self):
        return '<UserView %r - %r>' % (self.id, self.name)


class UserComponent(db.Model, ModelController, Ownable):
    component_id = db.Column(db.Integer, db.ForeignKey('component.id'))
    component = db.relationship("Component", backref='view_components')

    user_view_id = db.Column(db.Integer, db.ForeignKey('user_view.id'))
    user_view = db.relationship("UserView", backref='view_components')

    order = db.Column(db.Integer)
    columns = db.Column(db.Integer, default=12)
    options = db.Column(db.JSON)


class UserSettings(db.Model, ModelController, Ownable, Datable):
    last_context_id = db.Column(db.Integer, db.ForeignKey('context.id'))
    last_context = db.relationship("Context")

    background_img_url = db.Column(db.String)

    contexts_enabled = db.Column(db.Boolean, default=False)
    search_enabled = db.Column(db.Boolean, default=False)
    notifications_enabled = db.Column(db.Boolean, default=False)


class Notification(db.Model, ModelController, Datable, Deletable):
    message_template = db.Column(db.String)
    message_context = db.Column(db.JSON)

    url = db.Column(db.String)

    schedule_ts = db.Column(AwareDateTime())

    is_fired = db.Column(db.Boolean, default=False)
    fired_ts = db.Column(AwareDateTime())

    is_read = db.Column(db.Boolean, default=False)
    read_ts = db.Column(AwareDateTime())

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship("User", backref=db.backref("notifications"))

    def __repr__(self):
        return '<Notification %r - %r>' % (self.id, self.message_template)


FIELD_COUNT = 8


class FieldType(object):
    TYPE_SHORT_TEXT = u"SHORT_TEXT"
    TYPE_LONG_TEXT = u"LONG_TEXT"
    TYPE_INTEGER = u"INTEGER"
    TYPE_TIMER = u"TIMER"
    TYPE_CHECKBOX = u"CHECKBOX"
    TYPE_DATETIME = u"DATETIME"
    TYPE_TIME_PERIOD = u"TIME_PERIOD"
    TYPE_CHOICE_SET = u"CHOICE_SET"
    TYPE_RELATIONSHIP = u"RELATIONSHIP"


class ModelDefinition(db.Model, ModelController, Ownable, Datable):
    TYPE_OPTIONS = [
        (FieldType.TYPE_SHORT_TEXT, u'short_text'),
        (FieldType.TYPE_LONG_TEXT, u'long_text'),
        (FieldType.TYPE_INTEGER, u'integer'),
        (FieldType.TYPE_TIMER, u'timer'),
        (FieldType.TYPE_CHECKBOX, u'checkbox'),
        (FieldType.TYPE_DATETIME, u'datetime'),
        (FieldType.TYPE_TIME_PERIOD, u'time_period'),
        (FieldType.TYPE_CHOICE_SET, u'choice_set'),
        (FieldType.TYPE_RELATIONSHIP, u'relationship'),
    ]

    namespace = db.Column(db.Unicode)
    name = db.Column(db.Unicode)
    inherits = db.Column(db.Unicode)
    next_model_id = db.Column(db.Integer, default=1)

    field_01_label = db.Column(db.Unicode)
    field_01_type = db.Column(ChoiceType(TYPE_OPTIONS))
    field_01_options = db.Column(db.Unicode)

    field_02_label = db.Column(db.Unicode)
    field_02_type = db.Column(ChoiceType(TYPE_OPTIONS))
    field_02_options = db.Column(db.Unicode)

    field_03_label = db.Column(db.Unicode)
    field_03_type = db.Column(ChoiceType(TYPE_OPTIONS))
    field_03_options = db.Column(db.Unicode)

    field_04_label = db.Column(db.Unicode)
    field_04_type = db.Column(ChoiceType(TYPE_OPTIONS))
    field_04_options = db.Column(db.Unicode)

    field_05_label = db.Column(db.Unicode)
    field_05_type = db.Column(ChoiceType(TYPE_OPTIONS))
    field_05_options = db.Column(db.Unicode)

    field_06_label = db.Column(db.Unicode)
    field_06_type = db.Column(ChoiceType(TYPE_OPTIONS))
    field_06_options = db.Column(db.Unicode)

    field_07_label = db.Column(db.Unicode)
    field_07_type = db.Column(ChoiceType(TYPE_OPTIONS))
    field_07_options = db.Column(db.Unicode)

    field_08_label = db.Column(db.Unicode)
    field_08_type = db.Column(ChoiceType(TYPE_OPTIONS))
    field_08_options = db.Column(db.Unicode)

    def serialize(self):
        fields = []
        for num in range(1, FIELD_COUNT + 1):
            field = dict()
            label = getattr(self, "field_{:02d}_label".format(num))
            if label is not None:
                field["num"] = "{:02d}".format(num)
                field["label"] = label
                field_type = getattr(self, "field_{:02d}_type".format(num))
                field["type"] = field_type
                field["options"] = getattr(self, "field_{:02d}_options".format(num))
                fields.append(field)
        return {
            "name": self.name,
            "inherits": self.inherits,
            "fields": fields,
        }


class ModelInstance(db.Model, ModelController, Ownable, Datable):
    model_definition_id = db.Column(db.Integer,
                                    db.ForeignKey('model_definition.id'))
    model_definition = db.relationship("ModelDefinition",
                                       backref="model_instances")
    model_id = db.Column(db.Integer)
    field_01_value = db.Column(db.Unicode)
    field_02_value = db.Column(db.Unicode)
    field_03_value = db.Column(db.Unicode)
    field_04_value = db.Column(db.Unicode)
    field_05_value = db.Column(db.Unicode)
    field_06_value = db.Column(db.Unicode)
    field_07_value = db.Column(db.Unicode)
    field_08_value = db.Column(db.Unicode)


class RelationshipDefinition(db.Model, ModelController, Ownable, Datable):
    TYPE_OPTIONS = [
        ("MANY_TO_ONE", u'many_to_one'),
        ("MANY_TO_MANY", u'many_to_many'),
    ]

    from_model_definition_id = db.Column(db.Integer, db.ForeignKey('model_definition.id'))
    from_model_definition = db.relationship("ModelDefinition", backref="relationships_from",
                                            foreign_keys=[from_model_definition_id])

    to_model_definition_id = db.Column(db.Integer, db.ForeignKey('model_definition.id'))
    to_model_definition = db.relationship("ModelDefinition", backref="relationships_to",
                                          foreign_keys=[to_model_definition_id])

    relationship_name = db.Column(db.Unicode)
    relationship_backref = db.Column(db.Unicode)

    relationship_type = db.Column(ChoiceType(TYPE_OPTIONS))


class RelationshipInstance(db.Model, ModelController, Ownable, Datable):
    relationship_definition_id = db.Column(db.Integer,
                                           db.ForeignKey('relationship_definition.id'))
    relationship_definition = db.relationship("RelationshipDefinition",
                                              backref="relationship_definitions")

    from_model_instance_id = db.Column(db.Integer, db.ForeignKey('model_instance.id'))
    from_model_instance = db.relationship("ModelInstance", backref="relationships_from",
                                          foreign_keys=[from_model_instance_id])

    to_model_instance_id = db.Column(db.Integer, db.ForeignKey('model_instance.id'))
    to_model_instance = db.relationship("ModelInstance", backref="relationships_to",
                                        foreign_keys=[to_model_instance_id])


# ============= API
# ==============================================================================

from collections import defaultdict
import operator
import json


class Filter(object):
    EQUALS = operator.eq
    LESS_THAN = operator.lt
    GREATER_THAN = operator.gt

    def __init__(self, label=None, comparison=None, value=None):
        self.label = label
        self.comparison = comparison
        self.value = value


class Field(object):
    def __init__(self, label=None, type=None, options=None, value=None):
        self.label = label
        self.type = type
        self.options = options or "{}"
        self.value = value


class Model(object):
    @staticmethod
    def create_model(model_name, fields, model_inherits=None, namespace="default"):
        kwargs = dict()
        for num, field in enumerate(fields):
            num += 1
            assert isinstance(field, Field)
            kwargs["field_{:02d}_label".format(num)] = field.label
            kwargs["field_{:02d}_type".format(num)] = field.type
            kwargs["field_{:02d}_options".format(num)] = field.options

        model_definition = ModelDefinition.create(name=model_name,
                                                  namespace=namespace,
                                                  inherits=model_inherits,
                                                  **kwargs)
        return model_definition.serialize()

    @staticmethod
    def create_instance(model_name, fields, namespace="default"):
        model_definition = ModelDefinition.get_by(name=model_name, namespace=namespace)
        model_labels = {getattr(model_definition, "field_{:02d}_label".format(num)): num
                        for num in range(1, FIELD_COUNT + 1)}
        kwargs = dict()
        for field in fields:
            assert isinstance(field, Field)
            if field.label is not None:
                field_num = model_labels.get(field.label)
                if field_num:
                    kwargs["field_{:02d}_value".format(field_num)] = field.value

        model_instance = ModelInstance.create(
            model_id=model_definition.next_model_id,
            model_definition=model_definition,
            **kwargs)
        model_definition.next_model_id += 1
        db.session.add(model_definition)
        db.session.commit()
        return model_instance

    @classmethod
    def _filter(cls, model_definition, model_id=None, filters=None):
        model_labels = {getattr(model_definition, "field_{:02d}_label".format(num)): num
                        for num in range(1, FIELD_COUNT + 1)}
        if model_id:
            instances = [ModelInstance.get_by(model_definition=model_definition,
                                              model_id=model_id)]
        else:
            assert filters is not None
            resolved_filters = {}
            for filter in filters:
                num = model_labels.get(filter.label)
                resolved_filters["field_{:02d}_value".format(num)] = filter.value
            instances = ModelInstance.get_all_by(model_definition=model_definition,
                                                 **resolved_filters)
        return instances

    @classmethod
    def _update_instance(cls, model_definition, instance, updates):
        model_labels = {getattr(model_definition, "field_{:02d}_label".format(num)): num
                        for num in range(1, FIELD_COUNT + 1)}
        for field in updates:
            field_num = model_labels.get(field.label)
            if field_num:
                setattr(instance, "field_{:02d}_value".format(field_num), field.value)

        db.session.add(instance)

    @classmethod
    def _serialize_instance(cls, model_definition, instance):
        instance_fields = dict()
        for num in range(1, FIELD_COUNT + 1):
            label = getattr(model_definition, "field_{:02d}_label".format(num))
            if label is not None:
                value = getattr(instance, "field_{:02d}_value".format(num))
                instance_fields[label] = value
        return instance_fields

    @classmethod
    def query(cls, model_name, model_id=None, filters=None):
        model_definition = ModelDefinition.get_by(name=model_name)
        assert model_definition is not None

        instances = cls._filter(model_definition, model_id, filters)

        instances_serialized = list()
        for instance in instances:
            instances_serialized.append(cls._serialize_instance(model_definition, instance))
        return {
            "model": model_definition.serialize(),
            "instances": instances_serialized,
        }

    @classmethod
    def update(cls, model_name, updates, model_id=None, filters=None):
        model_definition = ModelDefinition.get_by(name=model_name)
        assert model_definition is not None

        instances = cls._filter(model_definition, model_id, filters)

        for instance in instances:
            cls._update_instance(model_definition, instance, updates)
        db.session.commit()
