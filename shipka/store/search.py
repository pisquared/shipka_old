"""
Use by decorating with `@whooshee.register_model('body')` for the models that need to be searchable.
Search by chaining `Entry.query.whooshee_search('something')`
"""
from flask_whooshee import Whooshee

whooshee = Whooshee()
