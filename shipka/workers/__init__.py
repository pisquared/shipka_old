import os
import sys

from celery import Celery
from flask import current_app

from shipka.webapp import create_app, config_app, init_common_components

if current_app:
    app = current_app
else:
    app = create_app()
    environment = os.environ.get('FLASK_WEBAPP_CONFIG') or 'development'

    config_app(app, environment)
    init_common_components(app)


def make_celery(app):
    celery = Celery(app.import_name, broker=app.config['CELERY_BROKER_URL'])
    celery.conf.result_backend = app.config['CELERY_RESULT_BACKEND']
    del app.config['CELERY_RESULT_BACKEND']
    celery.conf.update(app.config)
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery


celery = make_celery(app)

# register tasks
if 'shipka.workers.tasks' not in sys.modules.keys():
    pass

if 'workers.tasks' not in sys.modules.keys():
    from shipka.workers import tasks
