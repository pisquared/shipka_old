--- markdown.py	2018-01-25 00:25:39.687131598 +0100
+++ markdown_new.py	2018-01-25 00:20:32.284957000 +0100
@@ -25,11 +25,12 @@
 to register custom extensions by calling :func:`register_extension` or
 decorating the extension class with :func:`extend`
 
-:copyright: (c) 2010 by Dan Colish.
+:copyright: (c) 2013 by Dan Colish.
 :license: BSD, MIT see LICENSE for more details.
 """
 from __future__ import absolute_import
 from flask import Markup
+from jinja2 import evalcontextfilter, escape
 import markdown as md
 from markdown import (
     blockprocessors,
@@ -47,27 +48,54 @@
     for markdown may be passed as keyword arguments like so::
 
       md = Markdown(app,
+                    auto_escape=False,
                     extensions=['footnotes'],
                     extension_configs={'footnotes': ('PLACE_MARKER','~~~~~~~~')},
                     safe_mode=True,
-                    output_format='html4',
+                    output_format='html4'
                    )
 
     You can then call :func:`register_extension` to load custom extensions into
     the Markdown instance or use the :func:`extend` decorator
 
+    Additionally, passing auto_escape=True will cause the Markdown filter to
+    obey the Jinja2 auto_escape parameter in the context of the filter
+    evaluation. By default, this is disabled, and the content passed to the
+    Markdown filter will not be escaped.
+
     :param app: Your Flask app instance
+    :param bool auto_escape: Obey Jinja2 auto_escaping, defaults to False
     :param markdown_options: Keyword args for the Markdown instance
     """
 
-    def __init__(self, app, **markdown_options):
+    def __init__(self, app, auto_escape=False, **markdown_options):
         """Markdown uses old style classes"""
+        self.auto_escape = auto_escape
         self._instance = md.Markdown(**markdown_options)
-        app.jinja_env.filters.setdefault('markdown', self)
+        app.jinja_env.filters.setdefault(
+            'markdown', self.__build_filter(self.auto_escape))
 
     def __call__(self, stream):
         return Markup(self._instance.convert(stream))
 
+    def __build_filter(self, app_auto_escape):
+        @evalcontextfilter
+        def markdown_filter(eval_ctx, stream):
+            """
+            Called by Jinja2 when evaluating the Markdown filter. Utilizes the
+            Markdown instance stored in the Flask app config.
+
+            :param eval_ctx: Evaluation context from Jinja2, used for
+                             auto_escape
+            :param stream: Content passed to the filter
+            """
+            __filter = self
+            if app_auto_escape and eval_ctx.autoescape:
+                return Markup(__filter(escape(stream)))
+            else:
+                return Markup(__filter(stream))
+        return markdown_filter
+
     def extend(self, configs=None):
         """
         Decorator for registering macros
@@ -88,7 +116,8 @@
                                     '_begin')
                md.registerExtension(self)
 
-        :param configs: A dictionary of options for the extension being registered
+        :param configs: A dictionary of options for the extension being
+                        registered
         """
 
         def decorator(ext_cls):
@@ -110,7 +139,8 @@
         Any additional configuration arguments can be added to configs and will
         be passed through to the extension you are registering
 
-        :param configs: A dictionary of options for the extension being regsitered
+        :param configs: A dictionary of options for the extension being
+                        regsitered
         :param ext_cls: The class name of your extension
         """
         instance = ext_cls()
