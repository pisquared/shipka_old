syntax enable           " enable syntax processing
set expandtab
set shiftwidth=2
set softtabstop=2
set expandtab       " tabs are spaces
set number              " show line numbers
set showmatch           " highlight matching [{()}]
set incsearch           " search as characters are entered
set hlsearch            " highlight matches
set smartindent
filetype plugin indent on
set colorcolumn=80
