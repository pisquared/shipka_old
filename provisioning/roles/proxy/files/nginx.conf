server {
    listen 80;
    server_name {{ domain }};

    server_tokens off;
    charset utf-8;

    client_max_body_size 5M;

    access_log  /var/log/nginx/access.log;
    error_log  /var/log/nginx/error.log;

    location /socket.io {
        proxy_pass http://127.0.0.1:{{ app_server_local_port }}/socket.io;
        proxy_redirect off;
        proxy_buffering off;

        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";
    }

    location / {
        proxy_pass http://127.0.0.1:{{ app_server_local_port }};
        {% if is_dev is defined and is_dev %}
        proxy_redirect http://localhost http://localhost:{{ webapp_proxy_http_port }};
        {% else %}
        proxy_redirect off;
        {% endif %}

        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }
}
