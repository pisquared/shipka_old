* DEVOPS: Link shipka's front end shared javascript
* DOCS: draw diagram of separation of concerns

## v0.2
* DEVOPS: clone the appropriate shipka version as a part of provisioning
* DEVOPS: Config dev proxy static dir
* DEVOPS: Config prod proxy nginx and certbot
* DEVOPS: Prod provision - implement scripts in ansible
* DEVOPS: Enable install and save extra NPM packages
* DEVOPS: Init correctly with bootstrap/materialize - update fe_assets.json properly
* DEVOPS: front end setup is better via json and reusability increased

## v0.1 - Initial feature list
### Operations
* Easy provisioning with Vagrant / Virtualbox / Ansible roles
* Modular configuration:
    - Project-wide configuration is in `project_config.json`
    - Infrastructure configuration is in `Vagrantfile`
    - Host configuration is in `hosts.ini`
    - Per host application config is in `provisioning/playbook.yml`
    - WebApp configuration is in `config.py` with different environment settings for `DEV|TEST|PROD`
    - Production sensitive configuration is in `sensitive_config.json.sample` (remove `.sample` duh!)
* Optional Management interfaces for:
    - Postgres Database via `pgweb`
    - RabbitMQ Queue via `rabbitmq-management`
    - Celery Workers via `flower`
    - Admin console via `flask-admin`
    - Monitoring via `monit`
* Database versioning with `Flask-Migrations`

### Application stack
* Authentication with `Flask-Security`
* Database with `PostgreSQL`
* ORM with `Flask-SQLAlchemy`
* WebSockets with `Flask-SocketIO`
* Queue broker with `RabbitMQ`
* Async Workers with `Celery`
* Scheduled tasks with `Celery beat`
* Minifaction of assets with `Flask-Assets` for Javascript and CSS. For development pass the flag `--debug-fe` so that they are not minified.
* Pretty with `Bootstrap` or `Materialize`
* Icons with `Font Awesome`

### QA
* Test BDD with `lettuce` and helper setup functions and sentences

### Templates
* Create flask blueprints (subapps) with the `subapp` command
* Use Create/Update/Retrieve/Delete models with `Backbone`, save local entries with `Backbone-Localstorage` and receive live updates using `socketio`