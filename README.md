# Shipka - boilerplate DevOps-oriented WebApps based on Flask

##[Shipka is made of all of these](https://goo.gl/WGAoQR)

This project is a boilerplate for starting new webapp projects based on [Flask micro webframework](http://flask.pocoo.org/docs/0.12/) and [many others](https://goo.gl/WGAoQR). It provides you with easy setup, choose-what-you-need pluggable components that are nevertheless well integrated to work together. Shipka wants to follow closely the ideas behind [12 factor apps](https://12factor.net/). 

For front-end, shipka also integrates well with [CRUDy](https://gitlab.com/pisquared/crudy)

See [Features](https://gitlab.com/pisquared/shipka#features) below for more.

## Create a new project
If you wish to configure what elements will be included with this app:

```
./manage.sh configure
```

Then Run `./manage.sh bootstrap <PROJECT_PATH>`


This will create the needed files in the specified directory. Now to bootup the virtual machines, make sure you have [vagrant](https://www.vagrantup.com/downloads.html) and [virtualbox](https://www.virtualbox.org/wiki/Downloads)  for your operating system and run:

```
$ cd <PROJECT_PATH>
$ vagrant up  # this will start a virtual machine and provision it initially
```

Once you have it started, you can ssh to the machine:

```
$ vagrant ssh
```

Inside of the machine, it will put you directly in the app directory. There, `manage.sh` is your best friend. Run:

```
$ manage.sh status
```

To see the status of the running services.

To start a dev webserver with front end debugging enabled for example, run:

```
$ manage.sh runserver --fe-debug True
```

Run `manage.sh` without parameters to see help of other commands.

## Cloning existing projects based on shipka
If a project is dependant on [shipka](https://gitlab.com/pisquared/shipka) the steps are very similar:

 ```
 git clone <project_url>
 cd <project_name>
 ./manage.sh bootstrap    # this will clone the correct version of shipka and do some more initialization
 vagrant up
 ```

 Of course, read the `README.md` of the project itself - there might be pecularities.

## Configure
Take a look at `etc/project_config.json` to enable/disable features.

## Default Dev Passwords

| Admin tool | Address | Username | Password |
| ------------- | ------------- | ----- | ---- |
| Monitoring (Monit) | http://localhost:2812 | admin | monit |
| Admin console  | http://localhost:1337 | admin@app.com | password  |
| Postgres Database Mgmt (PgWeb) | http://localhost:8081 | localhost / {{ app_name }}_user / {{ app_name }}_dev_db | password  |
| Queue Mgmt (RabbitMQ) | http://localhost:15672 | admin | password |
| Workers Mgmt (Flower) | http://localhost:5555 | | |


## Features
### Operations
* Easy provisioning with Vagrant / Virtualbox / Ansible roles
* Modular configuration:
    - Project-wide configuration is in `etc/project_config.json`
    - Infrastructure configuration is in `Vagrantfile`
    - Host configuration is in `hosts.ini`
    - Per host application config is in `provisioning/playbook.yml`
    - WebApp configuration is in `config.py` with different environment settings for `DEV|TEST|PROD`
    - Production sensitive configuration is in `sensitive_config.json.sample` (remove `.sample` duh!)
* Optional Management interfaces for:
    - Postgres Database via `pgweb`
    - RabbitMQ Queue via `rabbitmq-management`
    - Celery Workers via `flower`
    - Admin console via `flask-admin`
    - Monitoring via `monit`
* Database versioning with `Flask-Migrations`

### Application stack
* Authentication with `Flask-Security`
* Database with `PostgreSQL`
* ORM with `Flask-SQLAlchemy`
* WebSockets with `Flask-SocketIO`
* Queue broker with `RabbitMQ`
* Async Workers with `Celery`
* Scheduled tasks with `Celery beat`
* Minifaction of assets with `Flask-Assets` for Javascript and CSS. For development pass the flag `--debug-fe` so that they are not minified.
* Pretty with `Bootstrap` or `Materialize`
* Icons with `Font Awesome`

### QA
* Test BDD with `lettuce` and helper setup functions and sentences

### Templates
* Create flask blueprints (subapps) with the `subapp` command
* Use Create/Update/Retrieve/Delete models with `Backbone`, save local entries with `Backbone-Localstorage` and receive live updates using `socketio`

### Shipka API
| methods |	JS	 | HTML |
| ------------- | ------------- | ----- | ---- |
| create render |		 | GET  /api/<models>/new |
| create |	POST   /api/<models> | 	POST /api/<models>/create |
| retrieve all |	GET    /api/<models> | 	GET  /api/<models>/all |
| retrieve single |	GET    /api/<models>/<id> | 	GET  /api/<models>/<id> |
| update render	 |	 | GET  /api/<models>/<id>/edit |
| update (change client side) |	PUT    /api/<models>/<id> | 	POST /api/<models>/<id>/update |
| update (descriptive) |	PATCH  /api/<models>/<id> | 	 |
| delete |	DELETE /api/<models>/<id> | 	POST /api/<models>/<id>/delete |
