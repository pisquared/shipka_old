#!/usr/bin/env bash
HELP="
Usage $0 [ assets | admin_webapp | beat | bootstrap | celery | db | db_connect| eventlet | flower | journal | pgweb | populate | prod_bootstrap | prod_push | recreate | reprov_project | runserver | shell | ssh | ssh_tunnel | status | sys | test | test_prod | translate | vm ]

    DEV ACTIVITIES:
    ===============
    bootstrap           Bootstrap the project by cloning shipka repo
    populate            Populates a database with initial values.
    recreate            Recreates everything - db, workers etc.
    reprov_project      Run the project specific provisioners only.
    test                Run BDD tests with lettuce
    test_prod           Setup a local prod machine for testing purposes
                          EXCEPT:
                             - setting up an SSL certificate
    translate           Generates translation files.

    ADMIN ACTIVITIES:
    ===============
    **!!Only if you have configured sensitive.py and have sensitive folder!!**
    prod_bootstrap      TODO Bootstrap Production - designed for bootstrapping before ansible can kick in. Works on Digital Ocean
    prod_push           Push to production:

    SERVICES:
    ===============
    sys                 alias to sudo systemctl #@
    journal             alias to sudo journalctl -u #@
    vm                  alias to vagrant #@
    runall              Run default development environment - server+worker+cron.
    runserver           Runs the server.
        --debug-fe=True      Enable debugging of front end by not compressing of static files.
        --debug-tb=True      Enable debug toolbar.
        --ssl=True           Enable running with certificates.
        prod                 Run in production.
    eventlet            Run eventlet server.
    celery              Run celery worker.
    beat                Run beat scheduler.
    db_connect          connect to the database
    ssh                 alias to vagrant ssh #@
    ssh_tunnel          creates a tunnel to all services

    ADMIN SERVICES:
    ===============
    admin_webapp        Run admin webapp.
    flower              Run flower management.
    pgweb               Run database management.

    MANAGEMENT:
    ===============
    assets              Manage static assets.
    db                  Perform database migrations.

    DEBUGING:
    ===============
    status              View the status of the installed and running services
    shell               Runs a Python shell inside Flask application context.
"

HELP_TRANSLATION="
USAGE ./manage.sh translate [extract|gen {lang}|compile|update]

    extract             Extract strings in files as defined in translations/babel.cfg
    gen {lang}          Init translations for {lang}
    compile             Compile all translations
    update              Use after a new extract - it may mark strings as fuzzy.
"

if [ ! -f etc/local_config.json ]; then
   echo "Path to clone/existing shipka repo (default: $(realpath $(pwd)/../shipka)): "
   read -r SHIPKA_PATH
   if [[ -z "$SHIPKA_PATH" ]]; then
        SHIPKA_PATH=$(realpath $(pwd)/../shipka)
   fi
   python -c "import json;json.dump({'local_shipka_path': '$SHIPKA_PATH'}, open('etc/local_config.json', 'w+'))"
else
  SHIPKA_PATH=`python -c 'import json;j=json.load(open("etc/local_config.json"));print(j["local_shipka_path"])'`
fi
SHIPKA_REMOTE_PATH=`python -c 'import json;j=json.load(open("etc/project_config.json"));print(j["remote_shipka_path"])'`

bootstrap() {
    [ ! -d "${SHIPKA_PATH}" ] && git clone https://gitlab.com/pisquared/shipka "${SHIPKA_PATH}"
    cp etc/sensitive_config.json.dev etc/sensitive_config.json
    VAGRANT_VERSION_EXPECTED="Vagrant 2.0.2"
    VAGRANT_VERSION=`vagrant --version`

    if [ ! "$VAGRANT_VERSION" == "$VAGRANT_VERSION_EXPECTED" ]; then
        echo "WARN: Vagrant version different than expected. It may still work but if there are any problems - Please install $VAGRANT_VERSION_EXPECTED - https://www.vagrantup.com/downloads.html"
    fi
}

command_db() {
    DB_COMMAND=$2
    case "$DB_COMMAND" in
        migrate) "${SHIPKA_REMOTE_PATH}/scripts/db_migrate.sh" "$@"
        ;;
        *) ./_manage.py "$@"
        ;;
    esac
    return $?
}

command_translate() {
    TRANSLATE_COMMAND=$2
    shift
    shift
    case "$TRANSLATE_COMMAND" in
        extract) pybabel extract -F translations/babel.cfg -o translations/messages.pot .
        ;;
        gen) pybabel init -i translations/messages.pot -d translations -l "$@"
        ;;
        compile) pybabel compile -d translations
        ;;
        update) pybabel update -i translations/messages.pot -d translations
        ;;
        *)  >&2 echo -e "${HELP_TRANSLATION}"
        ;;
    esac
    return $?
}

command_ssh_tunnel() {
    APP_PORT=`python -c 'import json;j=json.load(open("etc/project_config.json"));print(j["app_server_local_port"])'`
    DB_MGMT_PORT=`python -c 'import json;j=json.load(open("etc/project_config.json"));print(j["database_mgmt_port"])'`
    WRKS_MGMT_PORT=`python -c 'import json;j=json.load(open("etc/project_config.json"));print(j["workers_mgmt_port"])'`
    MONIT_MGMT_PORT=`python -c 'import json;j=json.load(open("etc/project_config.json"));print(j["monitoring_mgmt_port"])'`
    Q_MGMT_PORT=`python -c 'import json;j=json.load(open("etc/project_config.json"));print(j["queue_mgmt_port"])'`
    ERR_MGMT_PORT=`python -c 'import json;j=json.load(open("etc/project_config.json"));print(j["error_tracking_port"])'`
    ADMIN_PORT=`python -c 'import json;j=json.load(open("etc/project_config.json"));print(j["admin_webapp_port"])'`
    echo "connecting to: $MONIT_MGMT_PORT:localhost:$MONIT_MGMT_PORT -L $APP_PORT:localhost:$APP_PORT -L $ADMIN_PORT:localhost:$ADMIN_PORT -L $DB_MGMT_PORT:localhost:$DB_MGMT_PORT -L $WRKS_MGMT_PORT:localhost:$WRKS_MGMT_PORT -L $Q_MGMT_PORT:localhost:$Q_MGMT_PORT -L $ERR_MGMT_PORT:localhost:$ERR_MGMT_PORT"
    vagrant ssh -- -N -L $MONIT_MGMT_PORT:localhost:$MONIT_MGMT_PORT -L $APP_PORT:localhost:$APP_PORT -L $ADMIN_PORT:localhost:$ADMIN_PORT -L $DB_MGMT_PORT:localhost:$DB_MGMT_PORT -L $WRKS_MGMT_PORT:localhost:$WRKS_MGMT_PORT -L $Q_MGMT_PORT:localhost:$Q_MGMT_PORT -L $ERR_MGMT_PORT:localhost:$ERR_MGMT_PORT
}

db_connect() {
    APP_NAME=`python -c 'import json;j=json.load(open("etc/project_config.json"));print(j["app_name"])'`
    DB_USER=`python -c 'import json;j=json.load(open("etc/sensitive_config.json"));print(j["db_user"])'`
    psql -h localhost -p 5432 -U $DB_USER "${APP_NAME}_dev_db"
}

command_main() {
    INITIAL_COMMAND=$1
    case "$INITIAL_COMMAND" in
        assets|shell|runserver|runadmin|populate) ./_manage.py "$@"
        ;;
        admin_webapp) "${SHIPKA_REMOTE_PATH}/scripts/run_admin_webapp.sh" "$@"
        ;;
        runall) sudo systemctl stop celery beat && ./_manage.py "$@"
        ;;
        beat) "${SHIPKA_REMOTE_PATH}/scripts/run_beat.sh" "$@"
        ;;
        bootstrap) bootstrap
        ;;
        celery) "${SHIPKA_REMOTE_PATH}/scripts/run_celery.sh" "$@"
        ;;
        db) command_db "$@"
        ;;
        db_connect) db_connect
        ;;
        eventlet) "${SHIPKA_REMOTE_PATH}/scripts/run_eventlet.sh" "$@"
        ;;
        journal) shift; sudo journalctl -u "$@"
        ;;
        flower) "${SHIPKA_REMOTE_PATH}/scripts/run_flower.sh" "$@"
        ;;
        pgweb) "${SHIPKA_REMOTE_PATH}/scripts/run_pgweb.sh" "$@"
        ;;
        prod_bootstrap) "${SHIPKA_REMOTE_PATH}scripts/prod_bootstrap.sh" "$@"
        ;;
        prod_push) "${SHIPKA_REMOTE_PATH}/scripts/prod_push.sh" "$@"
        ;;
        recreate) "${SHIPKA_REMOTE_PATH}/scripts/recreate.sh"
        ;;
        reprov_project) vagrant provision --provision-with project_specific
        ;;
        status) shift && "${SHIPKA_REMOTE_PATH}/scripts/status.py" "$@"
        ;;
        ssh) shift; vagrant ssh "$@"
        ;;
        ssh_tunnel) shift; command_ssh_tunnel
        ;;
        sys) shift; sudo systemctl "$@"
        ;;
        test) lettuce
        ;;
        test_prod) "${SHIPKA_PATH}/scripts/test_prod.sh"
        ;;
        translate) command_translate "$@"
        ;;
        vm) shift; vagrant "$@"
        ;;
        *) >&2 echo -e "${HELP}"
        return 1
        ;;
    esac
    return $?
}

command_main "$@"
