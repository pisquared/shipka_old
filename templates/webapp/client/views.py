from flask import render_template
from shipka.webapp.views.crudy_integration import build_ctx

from webapp.client import client


@client.route('/')
def get_home():
    ctx = build_ctx()
    return render_template('index.html',
                           **ctx)
